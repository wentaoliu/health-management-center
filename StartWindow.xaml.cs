﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Data.SqlClient;
using HealthManagementCenter.Data;
using System.Windows.Media.Animation;

namespace HealthManagementCenter
{
    /// <summary>
    /// StartWindow.xaml 的交互逻辑
    /// </summary>
    public partial class StartWindow : Window
    {
        public StartWindow()
        {
            InitializeComponent();
            SqlConnection con = SqlHelper.Connection;
            try
            {
                con.Open();
            }
            catch
            {
                MessageBox.Show("数据库连接失败！", "失败", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }
            finally
            {
                con.Close();
            }
        }

        HealthDbEntities db = new HealthDbEntities();
         
        private void Login()
        {
            if (txtUserName.Text.Trim() == "" || txtPassword.Password.Trim() == "")
            {
                MessageBox.Show("请输入用户名和密码！", "提示", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            //确定登录身份
            MainWindow.UserRole role;
            if (rdbStudent.IsChecked == true)
            {
                role = MainWindow.UserRole.Student;
            }
            else if (rdbDoctor.IsChecked == true)
            {
                role = MainWindow.UserRole.Doctor;
            }
            else
            {
                role = MainWindow.UserRole.Admin;
            }
            string userName = txtUserName.Text.Trim();//用户名
            string password = txtPassword.Password.Trim();//密码
            string md5Password = DataHelper.GetMD5(password);//对密码进行MD5加密
            int val = 0;
            //验证用户有效性
            switch (role)
            {
                case MainWindow.UserRole.Student:
                    val = (int)SqlHelper.ExecuteScalar("exec LoginValidate @_num,@_pw,@_role", new SqlParameter("@_num", userName),
                            new SqlParameter("@_pw", md5Password), new SqlParameter("@_role", "student"));
                    break;
                case MainWindow.UserRole.Doctor:
                    val = (int)SqlHelper.ExecuteScalar("exec LoginValidate @_num,@_pw,@_role", new SqlParameter("@_num", userName),
                            new SqlParameter("@_pw", md5Password), new SqlParameter("@_role", "doctor"));
                    break;
                case MainWindow.UserRole.Admin:
                    val = (int)SqlHelper.ExecuteScalar("exec LoginValidate @_num,@_pw,@_role", new SqlParameter("@_num", userName),
                            new SqlParameter("@_pw", md5Password), new SqlParameter("@_role", "admin"));
                    break;
                default:
                    break;
            }

            if (val == 1)
            {
                //窗体变透明动画
                DoubleAnimation xAnimation = new DoubleAnimation();
                xAnimation.From = 1;
                xAnimation.To = 0.5;
                xAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
                Storyboard story = new Storyboard();
                story.Children.Add(xAnimation);
                Storyboard.SetTargetName(xAnimation, "start");
                Storyboard.SetTargetProperty(xAnimation, new PropertyPath("(Opacity)"));
                story.Begin(this);

                //检查用户是否使用默认密码（从未修改密码）
                if (DataHelper.GetMD5(password) == DataHelper.GetMD5(userName))
                {
                    MessageBox.Show("您仍在使用默认密码，请及时到个人信息管理中修改密码！", "提醒", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                }
                //显示欢迎图片
                SplashScreen s = new SplashScreen("/Content/Image/start.png");
                s.Show(false);
                s.Close(new TimeSpan(0, 0, 3));

                //启动主窗口
                MainWindow window = new MainWindow(userName, role);
                window.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("对不起，请检查您的用户名和密码是否正确", "错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        //支持窗体拖动
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            Point position = e.GetPosition(body);
            if (position.X < body.ActualWidth && position.Y < body.ActualHeight)
            {
                this.DragMove();
            }
        }

        private void Label_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key==Key.Enter)
            {
                Login();
            }
        }
    }
}
