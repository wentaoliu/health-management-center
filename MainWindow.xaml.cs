﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace HealthManagementCenter
{
    //定义一个委托，用于为医生用户显示一个新的就诊记录页面
    public delegate void NewVisitHandler(string snum);

    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Initialize

        public MainWindow(string name,UserRole role)
        {
            InitializeComponent();
            
            if (name == "")
                return;
            this._userName = name;
            this._role = role;
            //根据用户的不同显示不同的顶部导航内容
            switch(role)
            {
                case UserRole.Student:
                    this.panelStudent.Visibility = Visibility.Visible;
                    this.panelDoctor.Visibility = Visibility.Hidden;
                    this.panelAdmin.Visibility = Visibility.Hidden;
                    this.frame1.Content = new Pages.Student.HealthCondition(UserName);
                    this.lblRole.Content = "你的身份：学生";
                    this.lblNumber.Content = "你的学号：" + UserName;
                    this.lblName.Content = "你的姓名：" + db.Student.Where(s => s.snum == UserName).Single().sname;
                    this.lst4.Visibility = Visibility.Visible;
                    break;
                case UserRole.Doctor:
                    this.panelStudent.Visibility = Visibility.Hidden;
                    this.panelDoctor.Visibility = Visibility.Visible;
                    this.panelAdmin.Visibility = Visibility.Hidden;
                    Pages.Doctor.Appointment page = new Pages.Doctor.Appointment(UserName);
                    page.NewVisitEvent += new NewVisitHandler(NewVisit);
                    this.frame1.Content = page;
                    this.lblRole.Content = "您的身份：医生";
                    this.lblNumber.Content = "您的工号：" + UserName;
                    Data.Doctor d = db.Doctor.Where(a => a.docnum == UserName).Single();
                    this.lblName.Content = "您的姓名：" + d.docname;
                    this.lblDetpartment.Content = "所属科室：" + db.Department.Single(w => w.deptnum == d.deptnum).deptname;
                    this.lblDetpartment.Visibility = Visibility.Visible;
                    this.lst4.Visibility = Visibility.Hidden;
                    break;
                case UserRole.Admin:
                    this.panelStudent.Visibility = Visibility.Hidden;
                    this.panelDoctor.Visibility = Visibility.Hidden;
                    this.panelAdmin.Visibility = Visibility.Visible;
                    this.frame1.Content = new Pages.Admin.UserManage();
                    this.lblRole.Content = "您的身份：管理员";
                    this.lblNumber.Content = "您的编号：" + UserName;
                    this.lblName.Content = "您的姓名：" + db.Admin.Where(a=>a.anum==UserName).Single().aname;
                    this.lst4.Visibility = Visibility.Hidden;
                    break;
                default:
                    break;
            }     
        }
        

        #endregion

        #region Declarations

        Data.HealthDbEntities db = new Data.HealthDbEntities();
        
        //用户名
        private string _userName;
        public string UserName
        {
            get
            {
                return _userName;
            }
        }
        //用户角色
        private UserRole _role;
        public UserRole Role
        {
            get
            {
                return _role;
            }
        }
        //用户角色枚举值
        public enum UserRole:short
        {
            Student=0,  //学生
            Doctor=1,   //医生
            Admin=2     //管理员
        }

        #endregion

        #region UI

        // 支持标题栏拖动
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // 获取鼠标相对标题栏位置
            Point position = e.GetPosition(gridTitleBar);

            // 如果鼠标位置在标题栏内，允许拖动
            if (position.X < gridTitleBar.ActualWidth && position.Y < gridTitleBar.ActualHeight)
            {
                this.DragMove();
            }
        }
        //退出
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("确定要退出吗？","提示",MessageBoxButton.OKCancel)==MessageBoxResult.OK)
            {
                Application.Current.Shutdown();
            }
        }
        //最小化
        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        //修改密码
        private void btnUserInfo_Click(object sender, RoutedEventArgs e)
        {
            if(frameChangepw.Visibility==Visibility.Hidden)
            {
                Pages.ChangePassword page = new Pages.ChangePassword(UserName, Role);
                frameChangepw.Content = page;
                frameChangepw.Visibility = Visibility.Visible;
                columnLeft.Width = new GridLength(780);
                columnRight.Width = new GridLength(300);
            }
            else
            {
                frameChangepw.Visibility = Visibility.Hidden;
                columnLeft.Width = new GridLength(1080);
                columnRight.Width = new GridLength(0);
            }
        }
        //登出
        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("确定要登出吗？","提示",MessageBoxButton.OKCancel)==MessageBoxResult.OK)
            {
                StartWindow window = new StartWindow();
                window.Show();
                this.Close();
            }
        }
        //禁用鼠标滚轮滚屏
        private void OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
        }
        //导航栏按钮动画效果
        private void btnMouseEnter(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            Panel.SetZIndex(button, int.MaxValue);
        }
        private void btnMouseLeave(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            Panel.SetZIndex(button, 100);
        }

        #endregion

        #region ForStudent

        //健康状况
        private void btnStudentHealthCondition_Click(object sender, RoutedEventArgs e)
        {
            this.frame1.Content = new Pages.Student.HealthCondition(UserName);                   
            listbox.ScrollIntoView(lst1);
        }
        //锻炼计划
        private void btnExersicePlan_Click(object sender, RoutedEventArgs e)
        {
            this.frame2.Content = new Pages.Student.ExersicePlan(UserName);
            listbox.ScrollIntoView(lst2);
        }
        //门诊预约
        private void btnStudentAppointment_Click(object sender, RoutedEventArgs e)
        {
            this.frame3.Content = new Pages.Student.Appointment(UserName);
            listbox.ScrollIntoView(lst3); 
        }
        //历史记录
        private void btnStudentHistory_Click(object sender, RoutedEventArgs e)
        {
            this.frame4.Content = new Pages.Student.History(UserName);
            listbox.ScrollIntoView(lst4);           
        }

        #endregion

        #region ForDoctor
        //今日预约
        private void btnDoctorAppointment_Click(object sender, RoutedEventArgs e)
        {
            Pages.Doctor.Appointment page = new Pages.Doctor.Appointment(UserName);
            page.NewVisitEvent += new NewVisitHandler(NewVisit);
            this.frame1.Content = page;
            listbox.ScrollIntoView(lst1);    
        }
        //门诊记录
        private void btnDoctorVisit_Click(object sender, RoutedEventArgs e)
        {
            this.frame2.Content = new Pages.Doctor.Visit(UserName);
            listbox.ScrollIntoView(lst2);    
        }
        //历史记录
        private void btnDoctorHistory_Click(object sender, RoutedEventArgs e)
        {
            this.frame3.Content = new Pages.Doctor.History(UserName);
            listbox.ScrollIntoView(lst3);    
        }

        private void NewVisit(string snum)
        {
            this.frame2.Content = new Pages.Doctor.Visit(docnum:UserName,snum:snum);
            listbox.ScrollIntoView(lst2); 
        }
        #endregion

        #region ForAdmin
        //用户管理
        private void btnAdminUserManage_Click(object sender, RoutedEventArgs e)
        {
            this.frame1.Content = new Pages.Admin.UserManage();
            listbox.ScrollIntoView(lst1);    
        }
        //记录查询
        private void btnAdminQuery_Click(object sender, RoutedEventArgs e)
        {
            this.frame2.Content = new Pages.Admin.Query();
            listbox.ScrollIntoView(lst2);
        }
        //统计分析
        private void btnAdminStatistic_Click(object sender, RoutedEventArgs e)
        {
            this.frame3.Content = new Pages.Admin.Statistic();
            listbox.ScrollIntoView(lst3); 
        }
        #endregion

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(@"Help.html");
        }


    }
}
