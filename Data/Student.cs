//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HealthManagementCenter.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Student
    {
        public Student()
        {
            this.Appointment = new HashSet<Appointment>();
            this.Physical = new HashSet<Physical>();
            this.Visit = new HashSet<Visit>();
        }
    
        public string snum { get; set; }
        public string sname { get; set; }
        public string sex { get; set; }
        public Nullable<System.DateTime> birthday { get; set; }
        public string grade { get; set; }
        public string major { get; set; }
        public string password { get; set; }
        public Nullable<bool> admit { get; set; }
    
        public virtual ICollection<Appointment> Appointment { get; set; }
        public virtual ICollection<Physical> Physical { get; set; }
        public virtual ICollection<Visit> Visit { get; set; }
    }
}
