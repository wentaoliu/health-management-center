//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HealthManagementCenter.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Physical
    {
        public string snum { get; set; }
        public System.DateTime date { get; set; }
        public Nullable<decimal> height { get; set; }
        public Nullable<decimal> weight { get; set; }
        public Nullable<int> breathing { get; set; }
        public Nullable<decimal> grip { get; set; }
        public Nullable<int> jump { get; set; }
        public Nullable<int> score { get; set; }
    
        public virtual Student Student { get; set; }
    }
}
