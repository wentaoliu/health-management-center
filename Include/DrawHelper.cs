﻿using System;
using System.Linq;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace HealthManagementCenter
{
    public class DrawingHelper
    {
        //为管理员绘制统计图
        public static Bitmap DrawStatic(int width, int height, dynamic[] data)
        {
            int num = data.Length;

            Bitmap img = new System.Drawing.Bitmap(width, height);
            Graphics g = Graphics.FromImage(img);
            if (num < 2)
            {
                return img;
            }
            Pen pen1 = new Pen(Color.Black, 1);
            pen1.SetLineCap(LineCap.Flat, LineCap.ArrowAnchor, DashCap.Flat);

            //坐标轴
            g.DrawLine(pen1, 20, height - 20, width - 40, height - 20);
            g.DrawLine(pen1, 20, height - 20, 20, 20);
            //折线区域范围
            float avaliableWidht = width - 40;
            float avaliableHeight = (height - 40) * 2 / 3;
            //缩放比例
            float scale;
            if (data.Max(m => m.data) != data.Min(m => m.data))
                scale = (float)(avaliableHeight / (data.Max(m => m.data) - data.Min(m => m.data)));
            else
                scale = 0;
            PointF[] points = new PointF[num];

            //确定各点位置
            for (int i = 0; i < num; i++)
            {
                points[i].X = 20 + (avaliableWidht / (num * 2)) * (i * 2 + 1);
                if (scale == 0)
                    points[i].Y = height / 2;
                else
                    points[i].Y = (float)(height - 40 - avaliableHeight / 6 - (data[i].data - data.Min(m => m.data)) * scale);
            }
            //画折线
            Pen pen2 = new Pen(Color.Black, 3);
            for (int i = 0; i < num - 1; i++)
            {
                g.DrawLine(pen2, points[i], points[i + 1]);
            }

            Brush brush1 = new SolidBrush(Color.Gray);
            Brush brush2 = new SolidBrush(Color.Red);
            Font font = new Font("黑体", 14, FontStyle.Regular);
            //月份
            for (int i = 0; i < num; i++)
            {
                g.DrawString(data[i].month + "月", font, brush1, points[i].X, height - 18);
            }

            if (points[0].Y > points[1].Y)
                g.DrawString(data[0].data.ToString(), font, brush2, points[0].X - 20, points[0].Y + 5);
            else
                g.DrawString(data[0].data.ToString(), font, brush2, points[0].X - 20, points[0].Y - 15);

            if (points[num - 1].Y > points[num - 2].Y)
                g.DrawString(data[num - 1].data.ToString(), font, brush2, points[num - 1].X + 10, points[num - 1].Y + 5);
            else
                g.DrawString(data[num - 1].data.ToString(), font, brush2, points[num - 1].X + 10, points[num - 1].Y - 15);

            for (int i = 1; i < num - 1; i++)
            {
                if (points[i - 1].Y <= points[i].Y && points[i + 1].Y <= points[i].Y)
                {
                    g.DrawString(data[i].data.ToString(), font, brush2, points[i].X, points[i].Y + 5);
                }
                else if (points[i - 1].Y > points[i].Y && points[i + 1].Y > points[i].Y)
                {
                    g.DrawString(data[i].data.ToString(), font, brush2, points[i].X, points[i].Y - 15);
                }
                else
                {
                    double r1 = (points[i - 1].Y - points[i].Y) / (points[i - 1].X - points[i].X);
                    double r2 = (points[i].Y - points[i + 1].Y) / (points[i].X - points[i + 1].X);
                    if (r1 < r2)
                        g.DrawString(data[i].data.ToString(), font, brush2, points[i].X, points[i].Y + 5);
                    else
                        g.DrawString(data[i].data.ToString(), font, brush2, points[i].X, points[i].Y - 15);
                }
            }

            return img;
        }

        //为学生绘制数据变化图
        public static Bitmap DrawingPhysicals(int imgWidth, int imgHeight, Data.Physical[] physicals, PhysicalItem item)
        {
            int num = physicals.Length;
            Bitmap img = new Bitmap(imgWidth, imgHeight);
            if (num <2)
            {
                using (Graphics g = Graphics.FromImage(img))
                {
                    g.DrawString("对不起\r\n暂无历史数据\r\n或数据太少", new Font("微软雅黑", 16), new SolidBrush(Color.Gray), 30, 30);
                }
                return img;
            }

            double[] data = new double[num];

            Pen pen = null;
            switch (item)
            {
                case PhysicalItem.Height:
                    data = physicals.Select(p => Convert.ToDouble(p.height)).ToArray();
                    pen = new Pen(Brushes.Red, 3);
                    break;
                case PhysicalItem.Weight:
                    data = physicals.Select(p => Convert.ToDouble(p.weight)).ToArray();
                    pen = new Pen(Brushes.Blue, 3);
                    break;
                case PhysicalItem.Breathing:
                    data = physicals.Select(p => Convert.ToDouble(p.breathing)).ToArray();
                    pen = new Pen(Brushes.Green, 3);
                    break;
                case PhysicalItem.Grip:
                    data = physicals.Select(p => Convert.ToDouble(p.grip)).ToArray();
                    pen = new Pen(Brushes.Yellow, 3);
                    break;
                case PhysicalItem.Jump:
                    data = physicals.Select(p => Convert.ToDouble(p.jump)).ToArray();
                    pen = new Pen(Brushes.Pink, 3);
                    break;
                default:
                    break;
            }
            using (Graphics g = Graphics.FromImage(img))
            {
                Pen blackPen = new Pen(Brushes.Black, 1);
                //画坐标轴   
                blackPen.SetLineCap(LineCap.NoAnchor, LineCap.ArrowAnchor, DashCap.Flat);
                g.DrawLine(blackPen, new Point(20, imgHeight - 20), new Point(20, 20));
                g.DrawLine(blackPen, new Point(20, imgHeight - 20), new Point(imgWidth - 20, imgHeight - 20));

                PointF[] points = new PointF[num];

                float avaliableWidht = imgWidth - 40;
                float avaliableHeight = (imgHeight - 40) * 2 / 3;
                float scale;
                if (data.Max() != data.Min())
                    scale = (float)(avaliableHeight / (data.Max() - data.Min()));
                else
                    scale = 0;

                for (int i = 0; i < num; i++)
                {
                    //确定各点位置
                    points[i].X = 20 + (avaliableWidht / (2 * num)) * (i * 2 + 1);
                    points[i].Y = (float)(imgHeight - 20 - avaliableHeight / 6 - (data[i] - data.Min()) * scale);
                }

                //画折线
                for (int i = 0; i < num - 1; i++)
                {
                    g.DrawLine(pen, points[i], points[i + 1]);
                }

                Brush brush = new SolidBrush(Color.Gray);
                Font font = new Font("微软雅黑", 12);
                for (int i = 0; i < num; i++)
                {
                    g.DrawString(physicals[i].date.ToString("yyyy/MM"), font, brush, points[i].X, imgHeight - 18);
                }

                if (points[0].Y > points[1].Y)
                    g.DrawString(data[0].ToString(), font, brush, points[0].X - 30, points[0].Y + 5);
                else
                    g.DrawString(data[0].ToString(), font, brush, points[0].X - 30, points[0].Y - 5);

                if (points[num - 1].Y > points[num - 2].Y)
                    g.DrawString(data[num - 1].ToString(), font, brush, points[num - 1].X + 10, points[num - 1].Y + 5);
                else
                    g.DrawString(data[num - 1].ToString(), font, brush, points[num - 1].X + 10, points[num - 1].Y - 5);

                for (int i = 1; i < num - 1; i++)
                {
                    if (points[i - 1].Y <= points[i].Y && points[i + 1].Y <= points[i].Y)
                    {
                        g.DrawString(data[i].ToString(), font, brush, points[i].X, points[i].Y + 5);
                    }
                    else if (points[i - 1].Y > points[i].Y && points[i + 1].Y > points[i].Y)
                    {
                        g.DrawString(data[i].ToString(), font, brush, points[i].X, points[i].Y - 15);
                    }
                    else
                    {
                        double r1 = (points[i - 1].Y - points[i].Y) / (points[i - 1].X - points[i].X);
                        double r2 = (points[i].Y - points[i + 1].Y) / (points[i].X - points[i + 1].X);
                        if (r1 < r2)
                            g.DrawString(data[i].ToString(), font, brush, points[i].X, points[i].Y + 5);
                        else
                            g.DrawString(data[i].ToString(), font, brush, points[i].X, points[i].Y - 15);
                    }
                }

                return img;
            }
        }
    }

    public enum PhysicalItem : short
    {
        Height = 0,
        Weight = 1,
        Breathing = 2,
        Grip = 3,
        Jump = 4
    }
}
