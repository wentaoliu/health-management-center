﻿using System.Linq;
using System.Text;

using System.Security.Cryptography;//用于MD5加密

namespace HealthManagementCenter
{
    public static class DataHelper
    {
        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="strPwd">被加密的字符串</param>
        /// <returns>返回加密后的字符串</returns>
        public static string GetMD5(string strPwd)
        {
            string pwd = "";
            MD5 md5 = MD5.Create();
            byte[] s = md5.ComputeHash(Encoding.UTF8.GetBytes(strPwd));      
            s.Reverse();
            for (int i = 3; i < s.Length - 1; i++)
            {
                pwd = pwd + (s[i] < 198 ? s[i] + 28 : s[i]).ToString("X");
            }
            return pwd;
        }
    }

}
