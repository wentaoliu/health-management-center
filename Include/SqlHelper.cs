﻿using System.Data;
using System.Data.SqlClient;

namespace HealthManagementCenter
{
    /// <summary>
    /// SQL SERVER数据库操作通用类
    /// </summary>
    public static class SqlHelper
    {       
        public static SqlConnection Connection
        {
            get
            {
                return new SqlConnection(@"Data Source=.\SQLExpress;Initial Catalog=HealthDb;Integrated Security=True");
            }
        }

        /// <summary>
        /// 执行一个sql命令（不返回数据集）
        /// </summary>
        public static int ExecuteNonQuery(string cmdText, params SqlParameter[] commandParameters)
        {
            SqlCommand cmd = new SqlCommand();
            using (Connection)
            {
                int val;
                try
                {
                    PrepareCommand(cmd, Connection, null, cmdText, commandParameters);
                    val = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
                catch
                {
                    val = 0;
                }
                return val;
            }
        }

        /// <summary>
        /// 返回一个DataTable数据表
        /// </summary>
        public static DataTable ExecuteDataTable(string cmdText, params SqlParameter[] commandParameters)
        {
            //创建一个SqlCommand对象，并对其进行初始化
            SqlCommand cmd = new SqlCommand();
            using (Connection)
            {
                PrepareCommand(cmd, Connection, null, cmdText, commandParameters);
                //创建SqlDataAdapter对象以及DataSet
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                try
                {
                    //填充ds
                    da.Fill(dt);
                    // 清除cmd的参数集合 
                    cmd.Parameters.Clear();
                    //返回ds
                    return dt;
                }
                catch
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// 执行一个命令并返回一个数据集的第一行
        /// </summary>
        public static object ExecuteScalar(string cmdText, params SqlParameter[] commandParameters)
        {
            SqlCommand cmd = new SqlCommand();
            using (Connection)
            {
                PrepareCommand(cmd, Connection, null, cmdText, commandParameters);
                object val = cmd.ExecuteScalar();
                cmd.Parameters.Clear();
                return val;
            }
        }

        //准备命令
        private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, string cmdText, SqlParameter[] cmdParms)
        {
            //判断连接的状态。如果是关闭状态，则打开
            if (conn.State != ConnectionState.Open)
                conn.Open();
            //cmd属性赋值
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            //是否需要用到事务处理
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;
            //添加cmd需要的存储过程参数
            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }       
    }
}
