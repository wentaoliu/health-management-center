﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HealthManagementCenter.Resource
{
    public partial class NavButton
    {
        private void btnMouseEnter(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            Panel.SetZIndex(button, int.MaxValue);
        }

        private void btnMouseLeave(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            Panel.SetZIndex(button, 100);
        }
    }
}
