﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace HealthManagementCenter.Model
{
    public static class Doctor
    {
        public struct doctor
        {
            public string docnum { get; set; }
            public string docname { get; set; }
            public string deptnum { get; set; }
            public string deptname { get; set; }
        }

        public static doctor GetDoctor(string docnum)
        {
            string strQuery = "select docnum,docname,doctor.deptnum,deptname,password "
                + "from doctor,department where doctor.deptnum=department.deptnum and docnum=@docnum";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery, new SqlParameter("docnum", docnum));
            doctor d = new doctor();
            if (dt.Rows.Count == 0)
                return d;
            d.docnum = dt.Rows[0]["docnum"].ToString();
            d.docname = dt.Rows[0]["docname"].ToString();
            d.deptnum = dt.Rows[0]["deptnum"].ToString();
            d.deptname = dt.Rows[0]["deptname"].ToString();
            return d;
        }

        public static List<doctor> GetDoctorsList()
        {
            string strQuery = "select docnum,docname,doctor.deptnum,deptname,password "
                + "from doctor,department where doctor.deptnum=department.deptnum";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery);
            return GetList(dt);
        }

        private static List<doctor> GetList(DataTable dt)
        {
            List<doctor> list = new List<doctor>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                doctor d = new doctor();
                d.docnum = dt.Rows[0]["docnum"].ToString();
                d.docname = dt.Rows[0]["docname"].ToString();
                d.deptnum = dt.Rows[0]["deptnum"].ToString();
                d.deptname = dt.Rows[0]["deptname"].ToString();
                list.Add(d);
            }
            return list;
        }
    }
}
