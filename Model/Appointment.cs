﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace HealthManagementCenter.Model
{
    public static class Appointment
    {
        public struct appointment
        {
            public string snum { get; set; }
            public string sname { get; set; }
            public string deptnum { get; set; }
            public string deptname { get; set; }
            public DateTime date { get; set; }
            public string details { get; set; }
            public bool finished { get; set; }
        }

        public static int InsertNew(appointment a)
        {
            string strInsert="insert into appointment values(@snum,@deptnum,@date,@details,@finished)";
            int val = SqlHelper.ExecuteNonQuery(strInsert,
                new SqlParameter("snum", a.snum),
                new SqlParameter("deptnum", a.deptnum),
                new SqlParameter("date", a.date),
                new SqlParameter("details", a.details),
                new SqlParameter("finished", a.finished));
            return val;
        }

        public static List<appointment> GetAppointmentsListForStudent(string snum)
        {
            string strQuery = "select appointment.snum,sname,appointment.deptnum,deptname,date,details,finished "
                + "from appointment,student,department "
                + "where appointment.snum=student.snum and appointment.deptnum=department.deptnum and appointment.snum=@snum";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery,new SqlParameter("snum",snum));
            return GetList(dt);
        }

        public static List<appointment> GetAppointmentsListForDoctor(string docnum)
        {
            string strQuery = "select appointment.snum,sname,appointment.deptnum,deptname,date,details,finished "
                + "from appointment,student,department,doctor "
                + "where appointment.snum=student.snum and appointment.deptnum=department.deptnum "
                + "and doctor.deptnum=deptnum and doctor.docnum=@docnum";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery, new SqlParameter("docnum", docnum));
            return GetList(dt);
        }

        private static List<appointment> GetList(DataTable dt)
        {
            List<appointment> list = new List<appointment>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                appointment a = new appointment();
                a.snum = dt.Rows[i]["snum"].ToString();
                a.sname = dt.Rows[i]["sname"].ToString();
                a.deptnum = dt.Rows[i]["deptnum"].ToString();
                a.deptname = dt.Rows[i]["deptname"].ToString();
                a.date = Convert.ToDateTime(dt.Rows[i]["date"]);
                a.details = dt.Rows[i]["details"].ToString();
                a.finished = dt.Rows[i]["finished"] == null ? true : false;
                list.Add(a);
            }
            return list;
        }
    }
}
