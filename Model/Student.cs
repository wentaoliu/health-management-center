﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace HealthManagementCenter.Model
{
    public static class Student
    {
        public struct student
        {
            public string snum { get; set; }
            public string sname { get; set; }
            public string sex { get; set; }
            public DateTime birthday { get; set; }
            public string grade { get; set; }
            public string major { get; set; }
        }

        public static student GetStudent(string snum)
        {
            string strQuery = "select * from student where snum=@snum";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery,new SqlParameter("snum",snum));
            student s = new student();
            if (dt.Rows.Count == 0)
                return s;
            s.snum = dt.Rows[0]["snum"].ToString();
            s.sname = dt.Rows[0]["sname"].ToString();
            s.sex = dt.Rows[0]["sex"].ToString();
            s.birthday = Convert.ToDateTime(dt.Rows[0]["birthday"]);
            s.grade = dt.Rows[0]["grade"].ToString();
            s.major = dt.Rows[0]["major"].ToString();
            return s;
        }

        public static List<student> GetStudentsList()
        {
            string strQuery = "select * from student";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery);
            return GetList(dt);
        }

        private static List<student> GetList(DataTable dt)
        {
            List<student> list = new List<student>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                student s = new student();
                s.snum = dt.Rows[i]["snum"].ToString();
                s.sname = dt.Rows[i]["sname"].ToString();
                s.sex = dt.Rows[i]["sex"].ToString();
                s.birthday = Convert.ToDateTime(dt.Rows[i]["birthday"]);
                s.grade = dt.Rows[i]["grade"].ToString();
                s.major = dt.Rows[i]["major"].ToString();
                list.Add(s);
            }
            return list;
        }
    }
}
