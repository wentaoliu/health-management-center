﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace HealthManagementCenter.Model
{
    public static class Visit
    {
        public struct visit
        {
            public string snum { get; set; }
            public string sname { get; set; }
            public string deptnum { get; set; }
            public string deptname { get; set; }
            public DateTime date { get; set; }
            public string docnum { get; set; }
            public string docname { get; set; }
            public double price { get; set; }
            public double charged { get; set; }
            public string details { get; set; }
        }

        public static int InsertNew(visit v)
        {
            string strInsert = "insert into visit values(@snum,@deptnum,@date,@docnum,@price,@charged,@details)";
            int val = SqlHelper.ExecuteNonQuery(strInsert,
                new SqlParameter("snum", v.snum),
                new SqlParameter("deptnum", v.deptnum),
                new SqlParameter("date", v.date),
                new SqlParameter("docnum",v.docnum),
                new SqlParameter("price",v.price),
                new SqlParameter("charged",v.charged),
                new SqlParameter("details", v.details));
            return val;
        }

        public static visit GetLatestVisit(string snum,string docnum)
        {
            string strQuery = "select top 1 visit.snum,sname,visit.deptnum,deptname,date,visit.docnum,docname,price,charged,details "
                + "from visit,student,department,doctor "
                + "where visit.snum=student.snum and visit.deptnum=department.deptnum and visit.docnum=doctor.docnum "
                + "and visit.snum=@snum and visit.deptnum=(select deptnum from doctor where doctor.docnum=@docnum) order by date desc";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery, 
                new SqlParameter("snum", snum),
                new SqlParameter("docnum",docnum));    
            visit v = new visit();
            if (dt.Rows.Count == 0)
                return v;
            v.snum = dt.Rows[0]["snum"].ToString();
            v.sname = dt.Rows[0]["sname"].ToString();
            v.deptnum = dt.Rows[0]["deptnum"].ToString();
            v.deptname = dt.Rows[0]["deptname"].ToString();
            v.date = Convert.ToDateTime(dt.Rows[0]["date"]);
            v.docnum = dt.Rows[0]["docnum"].ToString();
            v.docname = dt.Rows[0]["docname"].ToString();
            v.price = Convert.ToDouble(dt.Rows[0]["price"]);
            v.charged = Convert.ToDouble(dt.Rows[0]["charged"]);
            v.details = dt.Rows[0]["details"].ToString();
            return v;
        }

        public static List<visit> GetVisitsListForStudent(string snum)
        {
            string strQuery = "select visit.snum,sname,visit.deptnum,deptname,date,visit.docnum,docname,price,charged,details "
                + "from visit,student,department,doctor "
                + "where visit.snum=student.snum and visit.deptnum=department.deptnum and visit.docnum=doctor.docnum "
                + "and visit.snum=@snum";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery, new SqlParameter("snum", snum));
            return GetList(dt);
        }

        public static List<visit> GetVisitsListForDoctor(string docnum)
        {
            string strQuery = "select visit.snum,sname,visit.deptnum,deptname,date,visit.docnum,docname,price,charged,details "
                + "from visit,student,department,doctor "
                + "where visit.snum=student.snum and visit.deptnum=department.deptnum and visit.docnum=doctor.docnum "
                + "and visit.docnum=@docnum";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery, new SqlParameter("docnum", docnum));
            return GetList(dt);
        }

        private static List<visit> GetList(DataTable dt)
        {
            List<visit> list = new List<visit>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                visit v = new visit();
                v.snum = dt.Rows[i]["snum"].ToString();
                v.sname = dt.Rows[i]["sname"].ToString();
                v.deptnum = dt.Rows[i]["deptnum"].ToString();
                v.deptname = dt.Rows[i]["deptname"].ToString();
                v.date = Convert.ToDateTime(dt.Rows[i]["date"]);
                v.docnum = dt.Rows[i]["docnum"].ToString();
                v.docname = dt.Rows[i]["docname"].ToString();
                v.price = Convert.ToDouble(dt.Rows[i]["price"]);
                v.charged = Convert.ToDouble(dt.Rows[i]["charged"]);
                v.details = dt.Rows[i]["details"].ToString();
                list.Add(v);
            }
            return list;
        }
    }
}
