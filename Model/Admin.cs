﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace HealthManagementCenter.Model
{
    public static class Admin
    {
        public struct admin
        {
            public string anum { get; set; }
            public string aname { get; set; }
        }

        public static admin GetAdmin(string anum)
        {
            string strQuery = "select * from admin where anum=@anum";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery, new SqlParameter("anum", anum));
            admin a = new admin();
            if (dt.Rows.Count == 0)
                return a;
            a.anum = dt.Rows[0]["anum"].ToString();
            a.aname = dt.Rows[0]["aname"].ToString();
            return a;
        }

        public static List<admin> GetAdminsList()
        {
            string strQuery = "select * from admin";
            DataTable dt = SqlHelper.ExecuteDataTable(strQuery);
            return GetList(dt);
        }

        private static List<admin> GetList(DataTable dt)
        {
            List<admin> list = new List<admin>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                admin a = new admin();
                a.anum = dt.Rows[i]["anum"].ToString();
                a.aname = dt.Rows[i]["aname"].ToString();
                list.Add(a);
            }
            return list;
        }
    }
}
