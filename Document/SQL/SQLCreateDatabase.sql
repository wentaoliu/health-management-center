USE master 
GO 
CREATE DATABASE HealthDb
ON 
( NAME = HealthDb_dat, 
    FILENAME = 'D:\Project\WPF\HealthManagementCenter\HealthManagementCenter\Data\HealthDb.mdf', 
    SIZE = 10MB, 
    MAXSIZE = 100MB,
    FILEGROWTH = 5MB )
 LOG ON 
( NAME = 'HealthDb_log',
    FILENAME = 'D:\Project\WPF\HealthManagementCenter\HealthManagementCenter\Data\HealthDb.log', 
    SIZE = 5MB, 
    MAXSIZE = 25MB,
    FILEGROWTH = 5MB ) 
GO 
