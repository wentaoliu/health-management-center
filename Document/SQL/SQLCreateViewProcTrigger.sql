--创建视图

--获取所有预约记录
create view vAppointment as
select appointment.snum,sname,appointment.deptnum,deptname,[date],details,finished
from appointment,student,department
where appointment.deptnum=department.deptnum and appointment.snum=student.snum
go

--获取所有就诊记录
create view vVisit as
select visit.snum,sname,doctor.deptnum,deptname,[date],visit.docnum,docname,price,charged,zs,xbs,jws,jc,cf
from visit,student,department,doctor
where doctor.deptnum=department.deptnum and visit.docnum=doctor.docnum and visit.snum=student.snum
go

--创建存储过程

--验证登陆
create proc LoginValidate
@_num char(7),
@_pw varchar(50),
@_role varchar(10)
as
begin 
if(@_role='student')
select count(*) from student where snum=@_num and [password]=@_pw and admit=1
else if(@_role='doctor') 
select count(*) from doctor where docnum=@_num and [password]=@_pw
else if(@_role='admin')
select count(*) from [admin] where anum=@_num and [password]=@_pw
end
go


--获取指定学号学生的预约记录
create proc GetAppointmentForStudent 
@_snum char(7) 
as
select *
from vAppointment
where snum=@_snum
order by [date] desc
go
--获取指定工号医生所在科室的预约记录
create proc GetAppointmentForDoctor 
@_docnum char(6)
as
select *
from vAppointment
where deptnum=(select deptnum from doctor where doctor.docnum=@_docnum)
and [date]>dateadd(dd,-1,getdate())
order by [date]
go

--删除指定预约记录
create proc DeleteAppointment
@_snum char(7),
@_deptnum char(4),
@_date datetime
as
delete from Appointment where snum=@_snum and deptnum=@_deptnum and year([date])=year(@_date)
and MONTH([date])=month(@_date) and day([date])=day(@_date)
go

--将指定的预约记录状态改为已完成
create proc ChangeAppointmentStatus
@_snum char(7),
@_deptnum char(4),
@_date datetime
as 
update Appointment set finished =1 where snum=@_snum and deptnum=@_deptnum and [date]=@_date
go 

--创建触发器


--学生表
create trigger iStudnet
on Student
for insert
as
declare @_snum as char(7)
select @_snum =i.snum from inserted as i
if (select count(*) from student where snum=@_snum)>1
begin
print '已存在该学号的学生'
rollback tran
end
else
print '成功'
go
--医生表
create trigger iDoctor
on Doctor
for insert
as
declare @_docnum as char(6)
select @_docnum =i.docnum from inserted as i
if (select count(*) from doctor where docnum=@_docnum)>1
begin
print '已存在该工号的医生'
rollback tran
end
else
print '成功'
go
--管理员表
create trigger iAdmin
on [Admin]
for insert
as
declare @_anum as char(4)
select @_anum =i.anum from inserted as i
if (select count(*) from [admin] where anum=@_anum)>1
begin
print '已存在该账号'
rollback tran
end
else
print '成功'
go
--添加新预约信息
create trigger iAppointment
on Appointment
for insert
as
declare @_snum as char(7)
declare @_deptnum as char(4)
select @_snum =i.snum from inserted as i
select @_deptnum =i.deptnum from inserted as i
if exists(select * from student where snum=@_snum) and exists(select * from department where deptnum=@_deptnum)
print '成功'
else
begin
print '未知的学号或科室号'
rollback tran
end
go
--添加新就诊信息
create trigger iVisit
on Visit
for insert
as
declare @_snum as char(7)
declare @_docnum as char(6)
select @_snum =i.snum from inserted as i
select @_docnum =i.docnum from inserted as i
if (exists(select * from student where snum=@_snum) 
and exists(select * from doctor where docnum=@_docnum))
print '成功'
else
begin
print '未知的学号或医生工号'
rollback tran
end
go
