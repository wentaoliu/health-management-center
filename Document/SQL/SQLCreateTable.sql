--创建health数据库库表

create table Student
(
snum char(7) primary key check(isnumeric(snum)=1 and(len(snum)=6 or len(snum)=7)),
sname varchar(30),
sex char(2) check(sex in('男','女')),
birthday date,
grade char(4) check(isnumeric(grade)=1),
major varchar(50),
[password] varchar(50),
admit bit default 1
)

create table Department
(
deptnum char(4) primary key check(left(deptnum,1) like ('[a-zA-Z]') and isnumeric(right(deptnum,3))=1),
deptname varchar(40)
)

create table Doctor
(
docnum char(6) primary key check(isnumeric(docnum)=1),
deptnum char(4) foreign key references department(deptnum),
docname varchar(20),
workday char(7),
[password] varchar(50)
)

create table Physical
(
snum char(7) foreign key references student(snum) on delete cascade,
[date] date,
height decimal(4,1) check(height between 0 and 250.0),
[weight] decimal(4,1) check([weight] between 0 and 500.0),
breathing int check(breathing between 0 and 10000),
grip decimal(3,1) check(grip between 0 and 99.9),
jump int check(jump between 0 and 500),
score int check(score between 0 and 100),
primary key(snum,[date])
)

create table [Admin]
(
anum char(4) primary key check(isnumeric(anum)=1),
aname varchar(20),
[password] varchar(50)
)

create table Visit
(
snum char(7) foreign key references student(snum) on delete cascade,
[date] datetime,
docnum char(6) foreign key references doctor(docnum) on delete cascade,
price money,
charged money,
zs text,
jws text,
xbs text,
jc text,
cf text,
primary key(snum,[date],docnum)
)

create table Appointment
(
snum char(7) foreign key references student(snum) on delete cascade,
deptnum char(4) foreign key references department(deptnum),
[date] datetime,
details text,
finished bit default 0,
primary key(snum,deptnum,[date])
)


