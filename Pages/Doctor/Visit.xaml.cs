﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Data.SqlClient;

using System.Text.RegularExpressions;
namespace HealthManagementCenter.Pages.Doctor
{
    /// <summary>
    /// Visit.xaml 的交互逻辑
    /// </summary>
    public partial class Visit : Page
    {
        public Visit(string docnum)
        {
            InitializeComponent();
            this._isAppointed = false;
            this._userName = docnum;
            SetValue();
        }

        public Visit(string docnum,string snum)
        {
            InitializeComponent();
            this._isAppointed = true;
            this._userName = docnum;
            SetValue();
            GetStudentInfo(snum);

        }
        //界面数据绑定
        private void SetValue()
        {
            Dictionary<string, double> discount = new Dictionary<string, double>
            {
                {"1折",0.1},
                {"2折",0.2},
                {"3折",0.3},
                {"5折",0.5},
                {"8折",0.8},
            };
            cmbDiscount.ItemsSource = discount;
            cmbDiscount.DisplayMemberPath = "Key";
            cmbDiscount.SelectedValuePath = "Value";
            cmbDiscount.SelectedIndex = 0;
            btnConfirm.Click += txtSnum_LostFocus;
            btnConfirm.Click += txtPrice_LostFocus;
        }

        Data.HealthDbEntities db = new Data.HealthDbEntities();

        private string _userName;
        private string UserName
        {
            get
            {
                return _userName;
            }
        }
        //是否来自于预约记录
        private bool _isAppointed;
        private bool IsAppointed
        {
            get
            {
                return _isAppointed;
            }
        }
        //根据学号获取学生信息
        private void GetStudentInfo(string snum)
        {
            var student = db.Student.Where(s => s.snum == snum).FirstOrDefault();
            if (student == null)
            {
                MessageBox.Show("对不起，该学号不存在");
                txtSnum.Text = "";
            }
            else
            {
                txtSnum.Text = snum;
                if (System.IO.File.Exists(@"Data/Photo/" + snum + ".jpg"))
                {
                    imgPhoto.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri("/Data/Photo/"+snum+".jpg", UriKind.Relative));
                }
                else
                {
                    imgPhoto.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri("/Content/Image/photo.png", UriKind.Relative));
                }
                lblName.Content = student.sname;
                lblMajor.Content = student.major;
                lblGrade.Content = student.grade;
                lblAge.Content = (DateTime.Now.Year - ((DateTime)student.birthday).Year) + "岁";
            }
        }

        private void txtSnum_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtSnum.Text.Length == 0)
            {
                lblName.Content = "";
                lblAge.Content = "";
                lblGrade.Content = "";
                lblMajor.Content = "";
                return;
            }   
            if (!((Regex.IsMatch(txtSnum.Text, @"[0-9]{6}") && txtSnum.Text.Length == 6) ||
                (Regex.IsMatch(txtSnum.Text, @"[0-9]{7}") && txtSnum.Text.Length == 7)))
            {
                MessageBox.Show("对不起，您输入的不是有效的学号");
                txtSnum.Text = "";
                return;
            }
            //对于符合规则的学号，查询并显示其详细信息
            string snum = txtSnum.Text.Trim();
            GetStudentInfo(snum);
        }

        private void txtPrice_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtPrice.Text.Length == 0)
            {
                lblCharged.Content = "";
                return;
            }     
            double price = 0d;
            //验证数字有效性
            if (double.TryParse(txtPrice.Text, out price))
            {
                double charged = price;
                charged *= (double)cmbDiscount.SelectedValue;
                lblCharged.Content = charged.ToString("0.00");
            }
            else
            {
                MessageBox.Show("请输入有效的数字");
            }
            
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            if(lblName.Content==null)
            {
                MessageBox.Show("请先输入有效的学号");
                return;
            }
            if(lblCharged.Content==null)
            {
                MessageBox.Show("请先输入有效的费用");
                return;
            }
            Data.Visit v = new Data.Visit()
            {
                snum = txtSnum.Text,
                date = DateTime.Now,
                docnum = UserName,
                price = decimal.Parse(txtPrice.Text),
                charged = decimal.Parse(lblCharged.Content.ToString()),
                zs=txtZs.Text.Length==0 ? "无":txtZs.Text,
                jws = txtJws.Text.Length == 0 ? "无" : txtJws.Text,
                xbs = txtXbs.Text.Length == 0 ? "无" : txtXbs.Text,
                jc = txtJc.Text.Length == 0 ? "无" : txtJc.Text,
                cf = txtCf.Text.Length == 0 ? "无" : txtCf.Text
            };

            db.Visit.Add(v);
            db.SaveChanges();
            //如果已经预约则将预约记录中的已完成改为true
            if(IsAppointed)
            {
                try
                {
                    string deptnum = db.Doctor.Where(d => d.docnum == UserName).First().deptnum;
                    SqlHelper.ExecuteNonQuery("exec ChangeAppointmentStatus @_snum,@_deptnum,@_date",
                        new SqlParameter("@_snum", v.snum), new SqlParameter("@_deptnum", deptnum), new SqlParameter("@_date", DateTime.Today));
                }
                catch
                {
                    MessageBox.Show("更新预约状态出现异常");
                }
            }
            txtSnum.Text = "";
            txtPrice.Text = "";
            txtZs.Text = "";
            txtXbs.Text = "";
            txtJws.Text = "";
            txtJc.Text = "";
            txtCf.Text = "";
        }

        
    }
}
