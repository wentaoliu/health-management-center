﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;


namespace HealthManagementCenter.Pages.Doctor
{
    /// <summary>
    /// History.xaml 的交互逻辑
    /// </summary>
    public partial class History : Page
    {
        public History(string docnum)
        {
            InitializeComponent();
            this._userName = docnum;
            this.Loaded += History_Loaded;
            
        }

        void History_Loaded(object sender, RoutedEventArgs e)
        {
            var visits = db.vVisit.Where(s => s.docnum == UserName).ToList();
            if (visits != null)
            {
                lvwVisit.ItemsSource = visits;
                this._list = visits;
            }
        }

        Data.HealthDbEntities db = new Data.HealthDbEntities();

        private string _userName;
        private string UserName
        {
            get
            {
                return _userName;
            }
        }
        //当前查询结果
        private List<Data.vVisit> _list;
        private List<Data.vVisit> List
        {
            get
            {
                return _list;
            }
        }

        private void dtpBeginDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpBeginDate.SelectedDate == null)
                return;
            if (dtpStopDate.SelectedDate != null)
            {
                if (dtpBeginDate.SelectedDate > dtpStopDate.SelectedDate)
                {
                    dtpBeginDate.SelectedDate = null;
                    MessageBox.Show("开始时间不能晚于结束时间");
                }
                else
                {
                    DateTime beginDate = (DateTime)dtpBeginDate.SelectedDate;
                    DateTime stopDate = (DateTime)dtpStopDate.SelectedDate;
                    var visits = List.Where(v => v.date >= beginDate && v.date <= stopDate).ToList();
                    lvwVisit.ItemsSource = visits;
                }
            }

        }

        private void dtpStopDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpStopDate.SelectedDate == null)
                return;
            if (dtpBeginDate.SelectedDate != null)
            {
                if (dtpStopDate.SelectedDate < dtpBeginDate.SelectedDate)
                {
                    dtpStopDate.SelectedDate = null;
                    MessageBox.Show("结束时间不能早于开始时间");
                }
                else
                {
                    DateTime beginDate = (DateTime)dtpBeginDate.SelectedDate;
                    DateTime stopDate = (DateTime)dtpStopDate.SelectedDate;
                    var visits = List.Where(v => v.date >= beginDate && v.date <= stopDate).ToList();
                    lvwVisit.ItemsSource = visits;
                }
            }
        }

    }
}
