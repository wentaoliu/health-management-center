﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Data.SqlClient;
using System.Data;

namespace HealthManagementCenter.Pages.Doctor
{
    /// <summary>
    /// Appointment.xaml 的交互逻辑
    /// </summary>
    public partial class Appointment : Page
    {
        public Appointment(string userName)
        {
            InitializeComponent();
            this._userName = userName;
            string deptnum = db.Doctor.Where(d => d.docnum == UserName).Single().deptnum;
            this.lvwAppointment.ItemsSource = ((DataTable)SqlHelper.ExecuteDataTable("exec GetAppointmentForDoctor @_docnum",
                new SqlParameter("@_docnum", userName))).DefaultView;
        }
        //用户名
        private string _userName;
        private string UserName
        {
            get
            {
                return _userName;
            }
        }

        Data.HealthDbEntities db = new Data.HealthDbEntities();

        private void lvwAppointment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvwAppointment.SelectedIndex < 0)
                return;
            string snum = ((DataRowView)lvwAppointment.SelectedValue)["snum"].ToString();
            string details = ((DataRowView)lvwAppointment.SelectedValue)["details"].ToString();
            txtDetails.Text = details;
            //获取学生身份信息
            var student = db.Student.Where(s => s.snum == snum).Single();
            this.lblSnum.Content = "学号：" + student.snum;
            this.lblName.Content = "姓名：" + student.sname;
            this.lblMajor.Content = "专业：" + student.major;
            this.lblGrade.Content = "年级：" + student.grade + "级";
            this.lblAge.Content = "年龄：" + (DateTime.Now.Year - ((DateTime)student.birthday).Year) + "岁";
            //获取学生最近就诊记录
            string deptnum = db.Doctor.Where(d => d.docnum == UserName).Single().deptnum;
            var visit = db.vVisit.Where(s=>s.snum==snum).OrderByDescending(t => t.date).FirstOrDefault();

            if (visit == null)
            {
                this.lblDate.Content = "时间：从未";
                this.lblDocName.Content = "医师：无";
                this.lblDetails.Text = "描述：无";
            }
            else
            {
                this.lblDate.Content = "时间：" + visit.date.ToString("yyyy年MM月dd日");
                this.lblDocName.Content = "科室：" + visit.deptname;
                this.lblDetails.Text = "主诉：" + visit.zs + "\r\n现病史：" + visit.xbs + "\r\n既往史："
                    + visit.jws + "\r\n检查：" + visit.jc + "\r\n处方：" + visit.cf +"习近平向好，全局主动习近平说，我到安徽代表团讲农村改革，是因为安徽是农业大省，长期以来为粮食安全和农村改革作出了重要贡献。要一如既往作抓好好，全局主动。农村改革是全面深化改革的重要组成部分，做好工作，关键在于向改革要活力。";
            }
        }

        public event NewVisitHandler NewVisitEvent;

        private void btnVisit_Click(object sender, RoutedEventArgs e)
        {
            if(lvwAppointment.SelectedIndex<0)
            {
                MessageBox.Show("请先选择一条记录", "提示", MessageBoxButton.OK);
                return;
            }
            DateTime date = Convert.ToDateTime(((DataRowView)lvwAppointment.SelectedValue)["date"]);
            if(date.Date!=DateTime.Today.Date)
            {
                MessageBox.Show("对不起今天不是预约日期！", "提示", MessageBoxButton.OK);
                return;
            }
            bool finished = Convert.ToBoolean(((DataRowView)lvwAppointment.SelectedValue)["finished"]);
            if(finished)
            {
                MessageBox.Show("该条预约已经完成");
                return;
            }
            string snum = ((DataRowView)lvwAppointment.SelectedValue)["snum"].ToString();
            NewVisitEvent(snum);
        }
    }
}
