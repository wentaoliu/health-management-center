﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace HealthManagementCenter.Pages.Admin
{
    /// <summary>
    /// Statistic.xaml 的交互逻辑
    /// </summary>
    public partial class Statistic : Page
    {
        public Statistic()
        {
            InitializeComponent();
            this.Loaded += Statistic_Loaded;
        }

        Data.HealthDbEntities db = new Data.HealthDbEntities();



        void Statistic_Loaded(object sender, RoutedEventArgs e)
        {
            lblDate.Content = "今天是" + DateTime.Today.ToString("yyyy年MM月dd日 ")
                + Enum.GetName(typeof(HealthManagementCenter.Pages.Student.Appointment.DayOfWeek), DateTime.Now.DayOfWeek);
            lblAppCount.Content = "今日预约数:" + db.Appointment.Where(m => m.date == DateTime.Today).Count();
            lblVisitCount.Content = "今日就诊数" + db.Visit.Where(m => m.date == DateTime.Today).Count();

            DateTime dt1 = DateTime.Today.AddMonths(-6);
            DateTime beginTime = new DateTime(dt1.Year, dt1.Month, 1, 0, 0, 0);//查询记录的开始时间
            DateTime endTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1, 0, 0, 0);//查询记录的结束时间


            //获取最近六个月就诊人数记录，没有记录的月份忽略不计
            var times = (from v in db.Visit
                         where v.date >= beginTime && v.date < endTime
                         orderby v.date
                         group v by v.date.Month into g
                         select new
                          {
                              month = g.FirstOrDefault().date.Month,
                              data = g.Count()
                          }).ToArray();
            //绘图
            img1.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                DrawingHelper.DrawStatic(600, 200,times).GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            //获取最近六个月总收入，没有记录的月份忽略不计
            var income = (from v in db.Visit
                          where v.date >= beginTime && v.date < endTime
                          orderby v.date
                          group v by v.date.Month into g
                          select new
                          {
                              month = g.FirstOrDefault().date.Month,
                              data = (double)g.Sum(a => a.charged)
                          }).ToArray();
            //绘图
            img2.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                DrawingHelper.DrawStatic(600, 200, income).GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());


        }

       
    }
}
