﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using HealthManagementCenter.Data;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace HealthManagementCenter.Pages.Admin
{
    /// <summary>
    /// UserManage.xaml 的交互逻辑
    /// </summary>
    public partial class UserManage : Page
    {
        public UserManage()
        {
            InitializeComponent();
            rdbStudent.IsChecked = true;
            txtSex.Items.Add("男");
            txtSex.Items.Add("女");
            txtSex.SelectedItem = "男";
            cmbDepartment.ItemsSource = db.Department.ToList();
            cmbDepartment.DisplayMemberPath = "deptname";
            cmbDepartment.SelectedValuePath = "deptnum";
        }

        private HealthDbEntities db = new HealthDbEntities();

        #region UI

        private void rdbStudent_Checked(object sender, RoutedEventArgs e)
        {
            lvwStudent.Visibility = Visibility.Visible;
            lvwDoctor.Visibility = Visibility.Collapsed;
            lvwAdmin.Visibility = Visibility.Collapsed;
            lvwStudent.ItemsSource = db.Student.Where(s => s.admit == true).ToList();
            splStudent.Visibility = Visibility.Visible;
            splDoctor.Visibility = Visibility.Collapsed;
            splAdmin.Visibility = Visibility.Collapsed;
            btnAdmitFalse.Visibility = Visibility.Visible;
            btnAdmitTure.Visibility = Visibility.Visible;
            txtAdmit.Visibility = Visibility.Visible;
            btnPhoto.Visibility = Visibility.Visible;
            ActiveTextBox();
        }

        private void rdbDoctor_Checked(object sender, RoutedEventArgs e)
        {
            lvwStudent.Visibility = Visibility.Collapsed;
            lvwDoctor.Visibility = Visibility.Visible;
            lvwAdmin.Visibility = Visibility.Collapsed;
            lvwDoctor.ItemsSource = db.Doctor.ToList();
            splStudent.Visibility = Visibility.Collapsed;
            splDoctor.Visibility = Visibility.Visible;
            splAdmin.Visibility = Visibility.Collapsed;
            btnAdmitFalse.Visibility = Visibility.Collapsed;
            btnAdmitTure.Visibility = Visibility.Collapsed;
            txtAdmit.Visibility = Visibility.Collapsed;
            btnPhoto.Visibility = Visibility.Collapsed;
            ActiveTextBox();
        }

        private void rdbAdmin_Checked(object sender, RoutedEventArgs e)
        {
            lvwStudent.Visibility = Visibility.Collapsed;
            lvwDoctor.Visibility = Visibility.Collapsed;
            lvwAdmin.Visibility = Visibility.Visible;
            lvwAdmin.ItemsSource = db.Admin.ToList();
            splStudent.Visibility = Visibility.Collapsed;
            splDoctor.Visibility = Visibility.Collapsed;
            splAdmin.Visibility = Visibility.Visible;
            btnAdmitFalse.Visibility = Visibility.Collapsed;
            btnAdmitTure.Visibility = Visibility.Collapsed;
            txtAdmit.Visibility = Visibility.Collapsed;
            btnPhoto.Visibility = Visibility.Collapsed;
            ActiveTextBox();
        }

        private void ActiveTextBox()
        {
            btnCancel.Visibility = Visibility.Hidden;
            btnEdit.Content = "修改";
            btnEdit.Click -= Save;
            btnEdit.Click += btnEdit_Click;
            txtSnum.IsEnabled = true;
            txtDocnum.IsEnabled = true;
            txtAnum.IsEnabled = true;
        }

        private void lvwStudent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvwStudent.SelectedIndex < 0)
                return;

            Data.Student student = (Data.Student)lvwStudent.SelectedItem;
            txtSnum.Text = student.snum;
            txtSname.Text = student.sname;
            txtSex.SelectedValue = student.sex;
            txtMajor.Text = student.major;
            txtGrade.Text = student.grade;
            dtpBirthday.SelectedDate = student.birthday;
        }

        private void lvwDoctor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvwDoctor.SelectedIndex < 0)
                return;

            Data.Doctor doctor = (Data.Doctor)lvwDoctor.SelectedItem;
            txtDocnum.Text = doctor.docnum;
            txtDocname.Text = doctor.docname;
            cmbDepartment.SelectedValue = doctor.deptnum;
            ClearWorkdays();
            //显示工作日
            //对workday字段中的值逐个判断显示
            foreach (char day in doctor.workday)
            {
                switch (day)
                {
                    case '1':
                        chkMonday.IsChecked = true;
                        break;
                    case '2':
                        chkTuesday.IsChecked = true;
                        break;
                    case '3':
                        chkWednesday.IsChecked = true;
                        break;
                    case '4':
                        chkThursday.IsChecked = true;
                        break;
                    case '5':
                        chkFriday.IsChecked = true;
                        break;
                    case '6':
                        chkSaturday.IsChecked = true;
                        break;
                    case '7':
                        chkSunday.IsChecked = true;
                        break;
                    default:
                        break;
                }
            }
        }

        private void lvwAdmin_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvwAdmin.SelectedIndex < 0)
                return;

            Data.Admin admin = (Data.Admin)lvwAdmin.SelectedItem;
            txtAnum.Text = admin.anum;
            txtAname.Text = admin.aname;
        }

        private void Clear()
        {
            txtSnum.Text = "";
            txtSname.Text = "";
            txtSex.SelectedIndex = 0;
            txtMajor.Text = "";
            txtGrade.Text = "";
            dtpBirthday.SelectedDate = null;
            txtDocnum.Text = "";
            txtDocname.Text = "";
            cmbDepartment.SelectedIndex = 0;

            ClearWorkdays();

            txtAnum.Text = "";
            txtAname.Text = "";
        }

        private void ClearWorkdays()
        {
            chkMonday.IsChecked = false;
            chkTuesday.IsChecked = false;
            chkWednesday.IsChecked = false;
            chkThursday.IsChecked = false;
            chkFriday.IsChecked = false;
            chkSaturday.IsChecked = false;
            chkSunday.IsChecked = false;
        }

        private string GetWorkdays()
        {
            StringBuilder sb = new StringBuilder();
            if (chkMonday.IsChecked == true)
                sb.Append("1");
            if (chkTuesday.IsChecked == true)
                sb.Append("2");
            if (chkWednesday.IsChecked == true)
                sb.Append("3");
            if (chkThursday.IsChecked == true)
                sb.Append("4");
            if (chkFriday.IsChecked == true)
                sb.Append("5");
            if (chkSaturday.IsChecked == true)
                sb.Append("6");
            if (chkSunday.IsChecked == true)
                sb.Append("7");
            if (sb.Length == 0)
                sb.Append("0");
            return sb.ToString();
        }

        #endregion

        #region 验证

        private bool ValidateForStudent()
        {
            if (!((Regex.IsMatch(txtSnum.Text, @"[0-9]{6}") && txtSnum.Text.Length == 6) ||
                (Regex.IsMatch(txtSnum.Text, @"[0-9]{7}") && txtSnum.Text.Length == 7)))
            {
                MessageBox.Show("对不起，学号应为6-7位的数字");
                return false;
            }
            if (txtSname.Text.Trim().Length == 0)
            {
                MessageBox.Show("对不起，姓名不可为空");
                return false;
            }
            if (txtMajor.Text.Trim().Length == 0)
            {
                MessageBox.Show("对不起，专业不可为空");
                return false;
            }
            if (!(Regex.IsMatch(txtGrade.Text, @"[0-9]{4}") && txtGrade.Text.Length == 4))
            {
                MessageBox.Show("对不起，不是有效的年份");
                return false;
            }
            if (dtpBirthday.SelectedDate == null)
            {
                MessageBox.Show("请选择一个有效的日期");
                return false;
            }
            return true;
        }

        private bool ValidateForDoctor()
        {
            if (!(Regex.IsMatch(txtDocnum.Text, @"[0-9]{6}") && txtDocnum.Text.Length == 6))
            {
                MessageBox.Show("对不起，工号应为6位数字");
                return false;
            }
            if (txtDocname.Text.Trim().Length == 0)
            {
                MessageBox.Show("对不起，姓名不可为空");
                return false;
            }
            if (cmbDepartment.SelectedIndex < 0)
            {
                MessageBox.Show("对不起，请选择一个科室");
                return false;
            }
            return true;
        }

        private bool ValidateForAdmin()
        {
            if (!(Regex.IsMatch(txtAnum.Text, @"[0-9]{4}") && txtAnum.Text.Length == 4))
            {
                MessageBox.Show("对不起，帐号应为4位数字");
                return false;
            }
            if (txtAname.Text.Trim().Length == 0)
            {
                MessageBox.Show("对不起，姓名不可为空");
                return false;
            }
            return true;
        }

        #endregion

        #region 新建

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            Clear();

            btnCancelNew.Visibility = Visibility.Visible;
            btnNew.Content = "保存";
            btnNew.Click -= btnNew_Click;
            btnNew.Click += AddNew;
        }

        private void btnCancelNew_Click(object sender, RoutedEventArgs e)
        {
            btnCancelNew.Visibility = Visibility.Hidden;
            btnNew.Content = "新建";
            btnNew.Click -= AddNew;
            btnNew.Click += btnNew_Click;
        }

        private void AddNew(object sender, RoutedEventArgs e)
        {
            if (rdbStudent.IsChecked == true)
            {
                if (!ValidateForStudent())
                    return;
                if (db.Student.Where(s => s.snum == txtSnum.Text).Count() > 0)
                {
                    MessageBox.Show("对不起,已存在该学号的学生");
                    return;
                }
                Data.Student student = new Data.Student()
                {
                    snum = txtSnum.Text,
                    sname = txtSname.Text,
                    sex = txtSex.SelectedValue.ToString(),
                    major = txtMajor.Text,
                    grade = txtGrade.Text,
                    birthday = (DateTime)dtpBirthday.SelectedDate,
                    password = DataHelper.GetMD5(txtSnum.Text),
                    admit = true
                };
                db.Student.Add(student);
                db.SaveChanges();
                lvwStudent.ItemsSource = db.Student.Where(s => s.admit == true).ToList();
            }
            else if (rdbDoctor.IsChecked == true)
            {
                if (!ValidateForDoctor())
                    return;
                if (db.Doctor.Where(s => s.docnum == txtDocnum.Text).Count() > 0)
                {
                    MessageBox.Show("对不起,已存在该工号的医生");
                    return;
                }
                Data.Doctor doctor = new Data.Doctor()
                {
                    docnum = txtDocnum.Text,
                    docname = txtDocname.Text,
                    deptnum = cmbDepartment.SelectedValue.ToString(),
                    workday = GetWorkdays(),
                    password = DataHelper.GetMD5(txtDocnum.Text)
                };
                db.Doctor.Add(doctor);
                db.SaveChanges();
                lvwDoctor.ItemsSource = db.Doctor.ToList();
            }
            else if (rdbAdmin.IsChecked == true)
            {
                if (!ValidateForAdmin())
                    return;
                if (db.Admin.Where(s => s.anum == txtAnum.Text).Count() > 0)
                {
                    MessageBox.Show("对不起,已存在该账号的管理员");
                    return;
                }
                int val = SqlHelper.ExecuteNonQuery("insert into [admin] values(@anum,@aname,@password)",
                    new SqlParameter("@anum", txtAnum.Text.Trim()),
                    new SqlParameter("@aname", txtAname.Text.Trim()),
                    new SqlParameter("@password", DataHelper.GetMD5(txtAnum.Text.Trim())));
                if(val==1)
                {
                    MessageBox.Show("添加成功！");
                    lvwAdmin.ItemsSource = db.Admin.ToList();
                }
                else
                {
                    MessageBox.Show("添加失败！");
                }
            }

            btnNew.Content = "新建";
            btnCancelNew.Visibility = Visibility.Collapsed;
            btnNew.Click -= AddNew;
            btnNew.Click += btnNew_Click;

            Clear();
        }
        #endregion

        #region 修改

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (rdbStudent.IsChecked == true)
            {
                if (lvwStudent.SelectedIndex<0)
                    return;
            }
            else if (rdbDoctor.IsChecked == true)
            {
                if (lvwDoctor.SelectedIndex<0)
                    return;
            }
            else if (rdbAdmin.IsChecked == true)
            {
                if (lvwAdmin.SelectedIndex<0)
                    return;
            }
            btnCancel.Visibility = Visibility.Visible;
            btnEdit.Content = "保存";
            btnEdit.Click -= btnEdit_Click;
            btnEdit.Click += Save;
            txtSnum.IsEnabled = false;
            txtDocnum.IsEnabled = false;
            txtAnum.IsEnabled = false;
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            if (btnEdit.Content.ToString() != "保存")
                return;
            if (rdbStudent.IsChecked == true)
            {
                if (!ValidateForStudent())
                    return;
                if (lvwStudent.SelectedIndex < 0)
                {
                    MessageBox.Show("请选择一条记录");
                    return;
                }
                Data.Student student = (Data.Student)lvwStudent.SelectedItem;
                Data.Student studentToEdit = db.Student.Find(student.snum);
                db.Student.Attach(studentToEdit);

                studentToEdit.sname = txtSname.Text;
                studentToEdit.sex = txtSex.SelectedValue.ToString();
                studentToEdit.major = txtMajor.Text;
                studentToEdit.grade = txtGrade.Text;
                studentToEdit.birthday = (DateTime)dtpBirthday.SelectedDate;

                db.Entry(studentToEdit).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                lvwStudent.ItemsSource = db.Student.Where(s => s.admit == true).ToList();
            }
            else if (rdbDoctor.IsChecked == true)
            {
                if (!ValidateForDoctor())
                    return;
                if (lvwDoctor.SelectedIndex < 0)
                {
                    MessageBox.Show("请选择一条记录");
                    return;
                }
                Data.Doctor doctor = (Data.Doctor)lvwDoctor.SelectedItem;
                Data.Doctor doctorToEdit = db.Doctor.Find(doctor.docnum);
                db.Doctor.Attach(doctorToEdit);

                doctorToEdit.docname = txtDocname.Text;
                doctorToEdit.deptnum = cmbDepartment.SelectedValue.ToString();
                doctorToEdit.workday = GetWorkdays();

                db.Entry(doctorToEdit).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                lvwDoctor.ItemsSource = db.Doctor.ToList();
            }
            else if (rdbAdmin.IsChecked == true)
            {
                if (!ValidateForAdmin())
                    return;
                if (lvwAdmin.SelectedIndex < 0)
                {
                    MessageBox.Show("请选择一条记录");
                    return;
                }
                Data.Admin admin = (Data.Admin)lvwAdmin.SelectedItem;
                Data.Admin adminToEdit = db.Admin.Find(admin.anum);
                db.Admin.Attach(adminToEdit);
                adminToEdit.aname = txtAname.Text;

                db.Entry(adminToEdit).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                lvwAdmin.ItemsSource = db.Admin.ToList();
            }
            ActiveTextBox();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            ActiveTextBox();
        }
        #endregion

        #region 恢复密码
        private void btnChangePw_Click(object sender, RoutedEventArgs e)
        {
            if (rdbStudent.IsChecked == true)
            {
                if (lvwStudent.SelectedIndex < 0)
                {
                    MessageBox.Show("请先选择一条学生记录");
                }
                else
                {
                    Data.Student student = (Data.Student)lvwStudent.SelectedItem;
                    try
                    {
                        Data.Student studentToChange = db.Student.Find(student.snum);
                        db.Student.Attach(studentToChange);
                        studentToChange.password = DataHelper.GetMD5(studentToChange.snum);
                        db.Entry(studentToChange).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        MessageBox.Show("恢复密码成功！");
                    }
                    catch
                    {
                        MessageBox.Show("恢复密码失败！");
                    }
                }
            }
            else if (rdbDoctor.IsChecked == true)
            {
                if (lvwDoctor.SelectedIndex < 0)
                {
                    MessageBox.Show("请先选择一条医生记录");
                }
                else
                {
                    Data.Doctor doctor = (Data.Doctor)lvwDoctor.SelectedItem;
                    try
                    {
                        Data.Doctor doctorToChange = db.Doctor.Find(doctor.docnum);
                        db.Doctor.Attach(doctorToChange);
                        doctorToChange.password = DataHelper.GetMD5(doctorToChange.docnum);
                        db.Entry(doctorToChange).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        MessageBox.Show("恢复密码成功！");
                    }
                    catch
                    {
                        MessageBox.Show("恢复密码失败！");
                    }
                }
            }
            else if (rdbAdmin.IsChecked == true)
            {
                if (lvwAdmin.SelectedIndex < 0)
                {
                    MessageBox.Show("请先选择一条管理员记录");
                }
                else
                {
                    Data.Admin admin = (Data.Admin)lvwAdmin.SelectedItem;
                    try
                    {
                        Data.Admin adminToChange = db.Admin.Find(admin.anum);
                        db.Admin.Attach(adminToChange);
                        adminToChange.password = DataHelper.GetMD5(adminToChange.anum);
                        db.Entry(adminToChange).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        MessageBox.Show("恢复密码成功！");
                    }
                    catch
                    {
                        MessageBox.Show("恢复密码失败！");
                    }
                }
            }
        }
        #endregion

        #region 删除

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("将同时删除所有相关的记录，确认继续吗？", "提示",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (rdbStudent.IsChecked == true)
                {
                    if (lvwStudent.SelectedIndex < 0)
                    {
                        MessageBox.Show("请先选择一条学生记录");
                    }
                    else
                    {
                        Data.Student student = (Data.Student)lvwStudent.SelectedItem;
                        int val = SqlHelper.ExecuteNonQuery("delete from student where snum=@snum", new SqlParameter("@snum", student.snum));
                        if (val == 1)
                        {
                            MessageBox.Show("删除成功！");
                        }
                        else
                        {
                            MessageBox.Show("删除失败！");
                        }
                    }
                    lvwStudent.ItemsSource = db.Student.Where(s => s.admit == true).ToList();
                }
                else if (rdbDoctor.IsChecked == true)
                {
                    if (lvwDoctor.SelectedIndex < 0)
                    {
                        MessageBox.Show("请先选择一条医生记录");
                    }
                    else
                    {
                        Data.Doctor doctor = (Data.Doctor)lvwDoctor.SelectedItem;
                        int val = SqlHelper.ExecuteNonQuery("delete from doctor where docnum=@docnum", new SqlParameter("@docnum", doctor.docnum));
                        if (val == 1)
                        {
                            MessageBox.Show("删除成功！");
                        }
                        else
                        {
                            MessageBox.Show("删除失败！");
                        }
                    }
                    lvwDoctor.ItemsSource = db.Doctor.ToList();
                }
                else if (rdbAdmin.IsChecked == true)
                {
                    if (lvwAdmin.SelectedIndex < 0)
                    {
                        MessageBox.Show("请先选择一条管理员记录");
                    }
                    else
                    {
                        if (lvwAdmin.Items.Count == 1)
                        {
                            MessageBox.Show("对不起，您是唯一一位管理员，不能删除！");
                            return;
                        }
                        Data.Admin admin = (Data.Admin)lvwAdmin.SelectedItem;
                        int val = SqlHelper.ExecuteNonQuery("delete from admin where anum=@anum", new SqlParameter("@anum", admin.anum));
                        if (val == 1)
                        {
                            MessageBox.Show("删除成功！");
                        }
                        else
                        {
                            MessageBox.Show("删除失败！");
                        }
                        lvwAdmin.ItemsSource = db.Admin.ToList();
                    }
                }
            }
        }

        #endregion

        #region 休学与复学

        private void btnAdmitFalse_Click(object sender, RoutedEventArgs e)
        {
            if (lvwStudent.SelectedIndex < 0)
            {
                MessageBox.Show("请先选择一条学生记录");
            }
            else
            {
                Data.Student student = (Data.Student)lvwStudent.SelectedItem;
                int val = SqlHelper.ExecuteNonQuery("update student set admit=0 where snum=@snum", new SqlParameter("@snum", student.snum));
                if (val == 1)
                {
                    MessageBox.Show("修改成功！");
                }
                else
                {
                    MessageBox.Show("修改失败！");
                }
            }
            lvwStudent.ItemsSource = db.Student.Where(s => s.admit == true).ToList();
        }

        private void btnAdmitTure_Click(object sender, RoutedEventArgs e)
        {
            string snum = txtAdmit.Text.Trim();
            if (snum.Length == 0)
            {
                MessageBox.Show("请输入要恢复的学号！");
                return;
            }
            int count = (int)SqlHelper.ExecuteScalar("select count(*) from student where snum=@snum and admit=0",
                                                    new SqlParameter("@snum", txtAdmit.Text.Trim()));
            if (count == 0)
            {
                MessageBox.Show("对不起，不存在该学号！");
                return;
            }
            int val = SqlHelper.ExecuteNonQuery("update student set admit=1 where snum=@snum", new SqlParameter("@snum", snum));
            if (val == 1)
            {
                MessageBox.Show("恢复成功！");
            }
            else
            {
                MessageBox.Show("恢复失败！");
            }
            lvwStudent.ItemsSource = db.Student.Where(s => s.admit == true).ToList();
        }

        #endregion

        private void btnPhoto_Click(object sender, RoutedEventArgs e)
        {
            if (lvwStudent.SelectedIndex < 0)
            {
                MessageBox.Show("请选择一条记录");
                return;
            }
            Data.Student student = (Data.Student)lvwStudent.SelectedItem;
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.Filter = "图像(JPEG)|*.jpg";
            ofd.ShowDialog();
            if (ofd.FileName != "")
            {
                if (System.IO.File.Exists(@"Data/Photo/" + student.snum + ".jpg"))
                {
                    if(MessageBox.Show("头像已存在，是否替换？","提示",MessageBoxButton.YesNo,MessageBoxImage.Question)!=MessageBoxResult.Yes)
                        return;
                    else
                        System.IO.File.Delete(@"Data/Photo/"+student.snum+".jpg");
                }
                System.IO.File.Copy(ofd.FileName, @"Data/Photo/" + student.snum + ".jpg");
                MessageBox.Show("头像更换成功！", "提示");
            }
        }
    }
}
