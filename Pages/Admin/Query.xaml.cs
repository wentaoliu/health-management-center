﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System;

namespace HealthManagementCenter.Pages.Admin
{
    /// <summary>
    /// Query.xaml 的交互逻辑
    /// </summary>
    public partial class Query : Page
    {
        public Query()
        {
            InitializeComponent();
            this._visits = db.vVisit.ToList();
            Result = this._visits;
            cmbDept.ItemsSource = db.Department.ToList();
            cmbDept.DisplayMemberPath = "deptname";
            cmbDept.SelectedValuePath = "deptnum";

            CalCountOfPages();
            ShowFirstPage();
        }
        private int countOfPages;
        private int currentPage = 0;
        private const int itemsPerPage = 15;
             
        public List<Data.vVisit> _visits;
        public List<Data.vVisit> Visits
        {
            get
            {
                return this._visits;
            }
        }

        public List<Data.vVisit> Result { get; set; }

        private Data.HealthDbEntities db = new Data.HealthDbEntities();

        //显示页面总数
        private void CalCountOfPages()
        {
            countOfPages = Result.Count() / itemsPerPage;
            if(Result.Count() % itemsPerPage!=0)
                countOfPages++;
            this.lblPageCount.Content = "共" + countOfPages + "页";
        }
        //显示第一页数据
        private void ShowFirstPage()
        {
            currentPage = 0;
            lvwVisit.ItemsSource = Result.OrderByDescending(v => v.date).Take(itemsPerPage).ToList();
            lblPageIndex.Content = "第1页";
        }
        //单击首页按钮
        private void btnPageFirst_Click(object sender, RoutedEventArgs e)
        {
            ShowFirstPage();
        }
        //单击前一页按钮
        private void btnPagePrevious_Click(object sender, RoutedEventArgs e)
        {
            currentPage--;
            if (currentPage < 0)
                currentPage = 0;
            lvwVisit.ItemsSource = Result.OrderByDescending(v => v.date).Skip(currentPage * itemsPerPage).Take(itemsPerPage).ToList();
            lblPageIndex.Content = "第" + (currentPage + 1) + "页";
        }
        //单击后一页按钮
        private void btnPageNext_Click(object sender, RoutedEventArgs e)
        {
            currentPage++;
            if (currentPage >= countOfPages)
                currentPage = countOfPages - 1;
            lvwVisit.ItemsSource = Result.OrderByDescending(v => v.date).Skip(currentPage * itemsPerPage).Take(itemsPerPage).ToList();
            lblPageIndex.Content = "第" + (currentPage + 1) + "页";
        }
        //单击尾页按钮
        private void btnPageLast_Click(object sender, RoutedEventArgs e)
        {
            currentPage = countOfPages - 1;
            lvwVisit.ItemsSource = Result.OrderByDescending(v => v.date).Skip(currentPage * itemsPerPage).Take(itemsPerPage).ToList();
            lblPageIndex.Content = "第" + countOfPages + "页";
        }
        //当选中一条记录时，显示详细信息
        private void lvwVisit_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvwVisit.SelectedIndex >= 0)
            {
                var visit = (Data.vVisit)lvwVisit.SelectedItem;
                txtDetails.Text = "主诉：" + visit.zs + "\r\n现病史：" + visit.xbs + "\r\n既往史："
                    + visit.jws + "\r\n检查：" + visit.jc + "\r\n处方：" + visit.cf;
            }
               
        }
        //查询记录
        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            Result = this._visits;
            if(chkSnum.IsChecked==true)
            {
                if(txtSnum.Text.Length==0)
                {
                    MessageBox.Show("请填写学号");
                    return;
                }
                Result = Result.Where(a => a.snum == txtSnum.Text.Trim()).ToList();
            }
            if (chkSname.IsChecked == true)
            {
                if (txtSname.Text.Length == 0)
                {
                    MessageBox.Show("请填写学生姓名");
                    return;
                }
                Result = Result.Where(a => a.sname == txtSname.Text.Trim()).ToList();
            }
            if (chkDocnum.IsChecked == true)
            {
                if (txtDocnum.Text.Length == 0)
                {
                    MessageBox.Show("请填写工号");
                    return;
                }
                Result = Result.Where(a => a.docnum == txtDocnum.Text.Trim()).ToList();
            }
            if(chkDocname.IsChecked==true)
            {
                if(txtDocname.Text.Length==0)
                {
                    MessageBox.Show("请输入医生姓名");
                    return;
                }
                Result = Result.Where(a => a.docname == txtDocname.Text.Trim()).ToList();
            }
            if (chkDept.IsChecked == true)
            {
                if (cmbDept.SelectedIndex<0)
                {
                    MessageBox.Show("请选择一个科室");
                    return;
                }
                Result = Result.Where(a => a.deptnum == cmbDept.SelectedValue.ToString()).ToList();
            }
            if (chkDate.IsChecked == true)
            {
                if (txtDate.SelectedDate==null)
                {
                    MessageBox.Show("请选择时间");
                    return;
                }
                Result = Result.Where(a => a.date.Date == txtDate.SelectedDate).ToList();
            }
            currentPage = 0;
            CalCountOfPages();
            ShowFirstPage();
        }

        //输出报表
        private void btnReport_Click(object sender, RoutedEventArgs e)
        {
            Result = this._visits;
            if (chkSnum.IsChecked == true)
            {
                if (txtSnum.Text.Length == 0)
                {
                    MessageBox.Show("请填写学号");
                    return;
                }
                Result = Result.Where(a => a.snum == txtSnum.Text.Trim()).ToList();
            }
            if (chkSname.IsChecked == true)
            {
                if (txtSname.Text.Length == 0)
                {
                    MessageBox.Show("请填写学生姓名");
                    return;
                }
                Result = Result.Where(a => a.sname == txtSname.Text.Trim()).ToList();
            }
            if (chkDocnum.IsChecked == true)
            {
                if (txtDocnum.Text.Length == 0)
                {
                    MessageBox.Show("请填写工号");
                    return;
                }
                Result = Result.Where(a => a.docnum == txtDocnum.Text.Trim()).ToList();
            }
            if (chkDocname.IsChecked == true)
            {
                if (txtDocname.Text.Length == 0)
                {
                    MessageBox.Show("请输入医生姓名");
                    return;
                }
                Result = Result.Where(a => a.docname == txtDocname.Text.Trim()).ToList();
            }
            if (chkDept.IsChecked == true)
            {
                if (cmbDept.SelectedIndex < 0)
                {
                    MessageBox.Show("请选择一个科室");
                    return;
                }
                Result = Result.Where(a => a.deptnum == cmbDept.SelectedValue.ToString()).ToList();
            }
            if (chkDate.IsChecked == true)
            {
                if (txtDate.SelectedDate == null)
                {
                    MessageBox.Show("请选择时间");
                    return;
                }
                Result = Result.Where(a => a.date.Date == txtDate.SelectedDate).ToList();
            }

            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            fbd.ShowDialog();
            if (fbd.SelectedPath != "")
            {

                //创建excel
                Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
                sheet.FreezePanes(1, 1, 1, 0);//冻结第一行

                // 第一行
                sheet.Cells["A1"].PutValue("学号");
                sheet.Cells["B1"].PutValue("学生姓名");
                sheet.Cells["C1"].PutValue("日期");
                sheet.Cells["D1"].PutValue("工号");
                sheet.Cells["E1"].PutValue("医生姓名");
                sheet.Cells["F1"].PutValue("科室号");
                sheet.Cells["G1"].PutValue("科室名");
                sheet.Cells["H1"].PutValue("主诉");
                sheet.Cells["I1"].PutValue("现病史");
                sheet.Cells["J1"].PutValue("既往史");
                sheet.Cells["K1"].PutValue("检查");
                sheet.Cells["L1"].PutValue("处方");

                // 循环写入内容
                int count = 1;
                foreach (var item in Result)
                {
                    count = count + 1;
                    sheet.Cells["A" + count].PutValue(item.snum);
                    sheet.Cells["B" + count].PutValue(item.sname);
                    sheet.Cells["C" + count].PutValue(item.date);
                    sheet.Cells["D" + count].PutValue(item.docnum);
                    sheet.Cells["E" + count].PutValue(item.docname);
                    sheet.Cells["F" + count].PutValue(item.deptnum);
                    sheet.Cells["G" + count].PutValue(item.deptname);
                    sheet.Cells["H" + count].PutValue(item.zs);
                    sheet.Cells["I" + count].PutValue(item.xbs);
                    sheet.Cells["J" + count].PutValue(item.jws);
                    sheet.Cells["K" + count].PutValue(item.jc);
                    sheet.Cells["L" + count].PutValue(item.cf);
                }

                //保存
                workbook.Save(fbd.SelectedPath + @"/就诊记录报表 " + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") +".xls");

            }
        }
    }
}
