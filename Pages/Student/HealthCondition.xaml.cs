﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using HealthManagementCenter.WeatherService;

namespace HealthManagementCenter.Pages.Student
{
    /// <summary>
    /// HealthCondition.xaml 的交互逻辑
    /// </summary>
    public partial class HealthCondition : Page
    {

        #region Initialize

        public HealthCondition(string name)
        {
            InitializeComponent();
            this._userName = name;
            string sex = db.Student.Where(s => s.snum == name).Single().sex;
            //默认图片为男性，如果性别为女则改用女生图片
            if(sex=="女")
            {
                imgMan.Source = new BitmapImage(new Uri("/Content/Image/girl.png", UriKind.Relative));
            }
            //获取最近三次体质测试数据
            this._historyPhysical = db.Physical.Where(s => s.snum == UserName).OrderBy(s => s.date).Take(3).ToArray();
            //异步获取在线体质测试数据
            GetOnlineDataHandler g = new GetOnlineDataHandler(GetPhysicalData);
            IAsyncResult iAR = g.BeginInvoke(new AsyncCallback(AddToDatabase), null);
            //获取天气信息
            GetWeather();
            //绘制身高变化趋势图
            GetDrawing(PhysicalItem.Height, "身高变化趋势");
        }

        #endregion

        #region Declarations

        public delegate void GetOnlineDataHandler();

        Data.HealthDbEntities db = new Data.HealthDbEntities();

        private Data.Physical _physical;
        private Data.Physical Physical
        {
            get
            {
                return _physical;
            }
        }

        private string _userName;
        private string UserName
        {
            get
            {
                return this._userName;
            }
        }

        private Data.Physical[] _historyPhysical; 
        private Data.Physical[] HistoryPhysical
        {
            get
            {
                return this._historyPhysical;
            }
        }

        #endregion

        #region WeatherReport

        //实例化天气预报web服务类
        private static WeatherWebServiceSoapClient ws = new WeatherWebServiceSoapClient("WeatherWebServiceSoap");

        //获取天气信息
        private async void GetWeather()
        {
            await Task.Run(() =>
            {
                try
                {
                    string[] weather = ws.getWeatherbyCityName("上海");
                    string today = weather[10].Substring(7).Replace("；", "\n");
                    string advice = weather[11];
                    Regex regex = new Regex(@"运动指数.*");
                    advice = regex.Match(advice).ToString();
                    //通过调用窗体的委托方法来实现跨线程的窗体更新
                    this.Dispatcher.Invoke((Action)delegate
                    {
                        lblWeather.Content = today;
                        lblAdvice.Text = advice;
                    });
                }
                catch
                {
                    this.Dispatcher.Invoke((Action)delegate
                    {
                        System.Windows.MessageBox.Show("对不起，链接天气服务失败，请检查网络连接", "提示");
                    });
                }
            });
        }

        #endregion

        #region WebBrowser

        private System.Windows.Forms.WebBrowser webBrowser = new System.Windows.Forms.WebBrowser();

        private async void GetPhysicalData()
        {
            await Task.Run(() =>
                {
                    try
                    {
                        this.Dispatcher.Invoke((Action)delegate
                        {
                            //使用System.Windows.Forms.WebBrowser获取学生锻炼次数和最新体质数据
                            webBrowser.Url = new Uri("http://www.tongji-pe.tongji.edu.cn/webscore/");//同济大学体育部网站
                            webBrowser.ScriptErrorsSuppressed = true;//禁用脚本错误
                            webBrowser.DocumentCompleted += webBrowser_DocumentCompleted;//订阅文档加载完成事件
                        });
                    }
                    catch
                    {
                        this.Dispatcher.Invoke((Action)delegate
                        {
                            System.Windows.MessageBox.Show("获取最新体质测试数据失败", "提示");
                        });
                    }
                });
        }

        //处理webBrowser文档加载完成事件
        private void webBrowser_DocumentCompleted(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            if (webBrowser.Url == new Uri("http://www.tongji-pe.tongji.edu.cn/webscore/"))
            {
                HtmlElement btnSubmit = webBrowser.Document.All["ImageButton1"];
                HtmlElement tbUserid = webBrowser.Document.All["txt_stid"];

                if (tbUserid == null || btnSubmit == null) return;
                tbUserid.SetAttribute("value", UserName);
                btnSubmit.InvokeMember("click");
            }
            else
            {
                HtmlElement xh = webBrowser.Document.GetElementById("lbl_exam1");//锻炼次数
                HtmlElement sg = webBrowser.Document.GetElementById("lbl_sg");//身高
                HtmlElement tz = webBrowser.Document.GetElementById("lbl_tz");//体重
                HtmlElement fhl = webBrowser.Document.GetElementById("lbl_fhl");//肺活量
                HtmlElement wl = webBrowser.Document.GetElementById("lbl_wl");//握力
                HtmlElement ldty = webBrowser.Document.GetElementById("lbl_ldty");//立定跳远
                HtmlElement zf = webBrowser.Document.GetElementById("lbl_Stotle");//总分
                this.lblExTimes.Content = xh.InnerText;
                Data.Physical physical = new Data.Physical()
                {
                    date = DateTime.Today,
                    height = int.Parse(sg.InnerText),
                    weight = int.Parse(tz.InnerText),
                    breathing = int.Parse(fhl.InnerText),
                    grip = decimal.Parse(wl.InnerText),
                    jump = int.Parse(ldty.InnerText),
                    score = int.Parse(zf.InnerText)
                };
                //在窗口显示
                ShowPhysicalData(physical);
                this._physical = physical;
            }
        }
        #endregion

        #region Database

        //将最新数据写入数据库
        private void AddToDatabase(IAsyncResult iAR)
        {
            //从数据库获取最近一条体质测试数据记录
            Data.Physical p = db.Physical.Where(s => s.snum == UserName).OrderByDescending(s => s.date).FirstOrDefault();
            //与当前从网页获取到的比较是否一致
            if (p == null)
                return;
            if (Physical == null)
                return;
            if ((p.height == Physical.height && p.weight == Physical.weight && p.jump == Physical.jump
                    && p.grip == Physical.grip && p.breathing == Physical.breathing)
                || p.date == DateTime.Today)
            {
                //如一致,或在线未获取到数据,或最新记录时间为今日，则直接返回
                return;
            }
            else
            {
                Physical.snum = UserName;//学号设为当前用户
                db.Physical.Add(Physical);//插入记录
                db.SaveChanges();
            }
        }
        #endregion

        #region UI
        //刷新获取最新记录
        private void btnLatest_Click(object sender, RoutedEventArgs e)
        {
            GetPhysicalData();
        }
     
        private void ShowPhysicalData(Data.Physical physical)  
        {
            lblDate.Content = "测试日期：" + physical.date.ToString("yyyy年MM月dd日");
            lblHeight.Content = "身高\r\n" + physical.height + "cm";
            lblWeigth.Content = "体重\r\n" + physical.weight + "kg";
            lblBreathing.Content = "肺活量\r\n" + physical.breathing + "ml";
            lblGrip.Content = "握力\r\n" + physical.grip + "N";
            lblJump.Content = "立定跳远\r\n" + physical.jump + "cm";
            int score = Convert.ToInt32(physical.score);
            if(score==0)
            {
                lblScore.Content = "对不起，暂时没有数据！";
            }
            else if (score < 60)
            {
                lblScore.Content = "你的体质测试成绩为不及格";
            }
            else if (score >= 60 && score < 75)
            {
                lblScore.Content = "你的体质测试成绩为及格";
            }
            else if (score >= 75 && score < 90)
            {
                lblScore.Content = "你的体质测试成绩为良好";
            }
            else if (score >= 90)
            {
                lblScore.Content = "你的体质测试成绩为优秀";
            }
        }

        private void lblHistoryHeight_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            GetDrawing(PhysicalItem.Height, "身高变化趋势");
        }
        
        private void lblHistoryWeight_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            GetDrawing(PhysicalItem.Weight, "体重变化趋势");
        }

        private void lblHistoryBreathing_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            GetDrawing(PhysicalItem.Breathing, "肺活量变化趋势");
        }

        private void lblHistoryGrip_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            GetDrawing(PhysicalItem.Grip, "握力变化趋势");
        }

        private void lblHistoryJump_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            GetDrawing(PhysicalItem.Jump, "跳远变化趋势");
        }
        
        private void GetDrawing(PhysicalItem item,string text)
        {
            drawing.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                DrawingHelper.DrawingPhysicals((int)drawing.Width,(int)drawing.Height, HistoryPhysical, item).GetHbitmap(),
                IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            lblPhysicalItem.Content = text;
        }
        #endregion
    }
}
