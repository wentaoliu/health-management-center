﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace HealthManagementCenter.Pages.Student
{
    /// <summary>
    /// History.xaml 的交互逻辑
    /// </summary>
    public partial class History : Page
    {
        public History(string userName)
        {
            InitializeComponent();
            this._userName = userName;
            this.Loaded += History_Loaded;
        }

        Data.HealthDbEntities db = new Data.HealthDbEntities();
        //用户名
        private string _userName;
        private string UserName
        {
            get
            {
                return _userName;
            }
        }
        //记录当前结果
        private List<Data.vVisit> _list;
        private List<Data.vVisit> List
        {
            get
            {
                return _list;
            }
        }

        void History_Loaded(object sender, RoutedEventArgs e)
        {
            BindSource();

            cmbDepartment.ItemsSource = db.Department.ToList();
            cmbDepartment.DisplayMemberPath = "deptname";
            cmbDepartment.SelectedValuePath = "deptnum";
        }

        private void BindSource()
        {
            var visits = db.vVisit.Where(s => s.snum == UserName).ToList();
            if (visits != null)
            {
                lvwVisit.ItemsSource = visits;
                this._list = visits;
            }
        }

        private void cmbDepartment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbDepartment.SelectedItem == null || List == null)
                return;
            string deptnum = cmbDepartment.SelectedValue.ToString();
            var visits = List.Where(v => v.deptnum == deptnum).ToList();
            lvwVisit.ItemsSource = visits;
        }

        private void dtpBeginDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpBeginDate.SelectedDate == null)
                return;
            if (dtpStopDate.SelectedDate != null)
            {
                if (dtpBeginDate.SelectedDate > dtpStopDate.SelectedDate)
                {
                    dtpBeginDate.SelectedDate = null;
                    MessageBox.Show("开始时间不能晚于结束时间");
                }
                else
                {
                    DateTime beginDate = (DateTime)dtpBeginDate.SelectedDate;
                    DateTime stopDate = (DateTime)dtpStopDate.SelectedDate;
                    var visits = List.Where(v => v.date >= beginDate && v.date <= stopDate).ToList();
                    lvwVisit.ItemsSource = visits;
                }
            }

        }

        private void dtpStopDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpStopDate.SelectedDate == null)
                return;
            if (dtpBeginDate.SelectedDate != null)
            {
                if (dtpStopDate.SelectedDate < dtpBeginDate.SelectedDate)
                {
                    dtpStopDate.SelectedDate = null;
                    MessageBox.Show("结束时间不能早于开始时间");
                }
                else
                {
                    DateTime beginDate = (DateTime)dtpBeginDate.SelectedDate;
                    DateTime stopDate = (DateTime)dtpStopDate.SelectedDate;
                    var visits = List.Where(v => v.date >= beginDate && v.date <= stopDate).ToList();
                    lvwVisit.ItemsSource = visits;
                }
            }
        }

        private void btnViewAll_Click(object sender, RoutedEventArgs e)
        {
            BindSource();
        }

    }
}
