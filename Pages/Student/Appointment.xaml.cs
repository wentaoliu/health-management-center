﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Windows.Media.Animation;
using System.Data.SqlClient;
using System.Data;

namespace HealthManagementCenter.Pages.Student
{
    /// <summary>
    /// Appointment.xaml 的交互逻辑
    /// </summary>
    public partial class Appointment : Page
    {
        public Appointment(string userName)
        {
            InitializeComponent();   
            this.Loaded += Appointment_Loaded;
            _userName = userName;
            cmbDepartment.SelectedIndex = 0;
            lstDept.ItemsSource = from d in XElement.Load(xmlPath).Elements("Department").Elements("Name")
                                  select d.Value;
        }

        Data.HealthDbEntities db = new Data.HealthDbEntities();

        //当前选择科室的工作日
        private bool[] workday = new bool[7];

        //帮助XML文档路径
        private const string xmlPath = @"Data/Department.xml";

        //用户名
        private string _userName;
        private string UserName
        {
            get
            {
                return _userName;
            }
        }

        private void Appointment_Loaded(object sender, RoutedEventArgs e)
        {
            BindSource();
            cmbDepartment.ItemsSource = db.Department.ToList();
            cmbDepartment.DisplayMemberPath = "deptname";
            cmbDepartment.SelectedValuePath = "deptnum";
        }
        //显示预约记录
        private void BindSource()
        {
            DataTable dt = SqlHelper.ExecuteDataTable("exec GetAppointmentForStudent @_snum", new SqlParameter("@_snum", UserName));
            lvwAppointment.ItemsSource = dt.DefaultView;
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            if(cmbDepartment.SelectedIndex<0)
            {
                MessageBox.Show("请先选择要预约的科室！");
                return;
            }
            if(gridWorkDay.SelectedIndex<0)
            {
                MessageBox.Show("请先选择要预约的时间！");
                return;
            }
            var row = (KeyValuePair<string,string>)gridWorkDay.SelectedItem;
            string[] num = row.Value.Split('/');
            if(num[0]==num[1])
            {
                MessageBox.Show("对不起，预约人数已满！");
                return;
            }
            if(txtDetails.Text.Length==0)
            {
                MessageBox.Show("您输入的详细信息太少！");
                return;
            }
            Data.Appointment a = new Data.Appointment()
            {
                snum = UserName,
                deptnum = cmbDepartment.SelectedValue.ToString(),
                date = DateTime.Now.AddDays(gridWorkDay.SelectedIndex + 1),
                details = txtDetails.Text,
                finished = false
            };
            db.Appointment.Add(a);
            try
            {
                db.SaveChanges();
                BindSource();//更新界面数据
                txtDetails.Text = "";
                cmbDepartment.SelectedIndex = -1;
            }
            catch
            {
                MessageBox.Show("你已经在当天预约过该科室，请不要重复预约！");
            }
            
        }
        //处理选择的科室改变事件
        private void cmbDepartment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbDepartment.SelectedValue == null)
                return;
            string department = cmbDepartment.SelectedValue.ToString();
            string[] workdays = (from dept in db.Department
                          join doc in db.Doctor
                          on dept.deptnum equals doc.deptnum
                          where dept.deptnum == department
                          select doc.workday).ToArray();

            int[] doctorPerDay = new int[7];
            
            foreach(string days in workdays)
            {
                foreach (var day in days.Trim())
                {
                    doctorPerDay[int.Parse(day.ToString())-1]++;
                }
            }

            const int numPerDoctor = 20;//每个医生可预约人数

            Dictionary<string, string> workdayToShow = new Dictionary<string, string>();

            for (int i = 0; i < 7;i++ )
            {
                DateTime beginDate=DateTime.Now.AddDays(i + 1);
                DateTime endDate = DateTime.Now.AddDays(i + 2);
                workdayToShow.Add(
                    beginDate.ToString("MM/dd") + " " + Enum.GetName(typeof(DayOfWeek), beginDate.DayOfWeek),
                    (from a in db.vAppointment
                     where a.deptnum == department && a.date >= beginDate.Date && a.date < endDate.Date
                     select a).Count() + "/" + doctorPerDay[i] * numPerDoctor);
            }
            gridWorkDay.DataContext = workdayToShow;

        }
        //一周星期的枚举值
        public enum DayOfWeek
        {
            星期天 = 0,
            星期一 = 1,
            星期二 = 2,
            星期三 = 3,
            星期四 = 4,
            星期五 = 5,
            星期六 = 6,
        } 

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if(lvwAppointment.SelectedIndex<0)
            {
                MessageBox.Show("请先选择一条记录！");
                return;
            }
            bool finished = (bool)((DataRowView)lvwAppointment.SelectedValue)["finished"];
            if(finished)
            {
                MessageBox.Show("对不起，已完成的预约不可取消");
                return;
            }
            string deptnum = ((DataRowView)lvwAppointment.SelectedValue)["deptnum"].ToString();
            string date = ((DateTime)((DataRowView)lvwAppointment.SelectedValue)["date"]).ToString("yyyy-MM-dd");

            if(SqlHelper.ExecuteNonQuery("exec DeleteAppointment @_snum,@_deptnum,@_date", new SqlParameter("@_snum", UserName),
                new SqlParameter("@_deptnum", deptnum), new SqlParameter("@_date", date))==1)
            {
                MessageBox.Show("删除成功！");
                BindSource();
            }
            else
            {
                MessageBox.Show("啊哦！出错了");
            }
        }

        private void lstDept_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstDept.SelectedIndex >= 0)
            {
                string dept = lstDept.SelectedItem.ToString();
                lblDeptName.Content = dept;
                var description = (from d in XElement.Load(xmlPath).Elements("Department")
                                   where d.Element("Name").Value == dept
                                   select d.Element("Description").Value).First();
                lblDeptDesc.Text = description.ToString();
            }
        }

        //动画显示与隐藏帮助内容
        private void lblSuggestion_Click(object sender, RoutedEventArgs e)
        {
            if (help.Visibility==Visibility.Collapsed)
            {
                help.Visibility = Visibility.Visible;
                DoubleAnimation xAnimation = new DoubleAnimation();
                xAnimation.From = -520;
                xAnimation.To = 10;
                xAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
                Storyboard story = new Storyboard();
                story.Children.Add(xAnimation);
                Storyboard.SetTargetName(xAnimation, "help");
                Storyboard.SetTargetProperty(xAnimation, new PropertyPath("(Canvas.Left)"));
                story.Begin(this);
            }
            else
            {
                DoubleAnimation xAnimation = new DoubleAnimation();
                xAnimation.From = 10;
                xAnimation.To = -520;
                xAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
                Storyboard story = new Storyboard();
                story.Children.Add(xAnimation);
                Storyboard.SetTargetName(xAnimation, "help");
                Storyboard.SetTargetProperty(xAnimation, new PropertyPath("(Canvas.Left)"));
                story.Completed += delegate
                {
                    help.Visibility = Visibility.Collapsed;
                };
                story.Begin(this);
            }
        }

        private void gridWorkDay_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
        }
    }
}
