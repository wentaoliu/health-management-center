﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HealthManagementCenter.Pages.Student
{
    /// <summary>
    /// ExersicePlan.xaml 的交互逻辑
    /// </summary>
    public partial class ExersicePlan : Page
    {
        public ExersicePlan(string userName)
        {
            InitializeComponent();
            _userName = userName;
            this.dtpExercise.SelectedDate = DateTime.Now;
            this.dtpExercise.DisplayDateStart = DateTime.Now.AddDays(-4);
            this.dtpExercise.DisplayDateEnd = DateTime.Now.AddDays(+7);
            DeleteOutodDateXML();
            BindSource();
        }

        private void exerDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            BindSource();
        }
        //添加计划
        private void btAddDetail_Click(object sender, RoutedEventArgs e)
        {
            if (tbAddDetail.Text != "")
            {
                XElement xe = XElement.Load(xmlPath);
                XElement add = new XElement("Exercise", new XElement("Name", _userName),
                                                        new XElement("Date", DatePickerFormatChange(dtpExercise)),
                                                        new XElement("Detail", tbAddDetail.Text),
                                                        new XElement("Finished", 0));
                xe.Add(add);
                xe.Save(xmlPath);
                BindSource();
                tbAddDetail.Text = "";
            }
            else
            {
                MessageBox.Show("请您输入您的计划！");
                tbAddDetail.Focus();
            }
        }
        //删除选定计划
        private void btDeleteDetail_Click(object sender, RoutedEventArgs e)
        {
            lvwDetails.SelectedItem = ((Button)sender).DataContext;
            string target = lvwDetails.SelectedItem.ToString();
            target = target.Remove(0, 11);
            target = target.Replace(" }", "");
            XElement xe = XElement.Load(xmlPath);
            IEnumerable<XElement> delete = from del in xe.Elements("Exercise")
                                           where del.Element("Detail").Value.ToString() == target
                                           && del.Element("Name").Value.ToString() == _userName
                                           && del.Element("Date").Value.ToString() == DatePickerFormatChange(dtpExercise)
                                           select del;
            if (delete.Count() > 0)
            {
                delete.First().Remove();
            }
            xe.Save(xmlPath);
            BindSource();
        }

        private string _userName;
        private string UserName
        {
            get
            {
                return _userName;
            }
        }

        //XML文档路径
        private const string xmlPath = @"Data/Exercise.xml";
        //绑定ListView
        private void BindSource()
        {
            lvwDetails.ItemsSource = from ex in XElement.Load(xmlPath).Elements("Exercise")
                                     where ex.Element("Name").Value.ToString() == _userName
                                     && ex.Element("Date").Value.ToString() == DatePickerFormatChange(dtpExercise)
                                     select new { Detail = ex.Element("Detail").Value };
        }
        //转换DatePicker输出格式
        private static string DatePickerFormatChange(DatePicker datePicker)
        {
            DateTime dt = (DateTime)datePicker.SelectedDate;
            string strDate = dt.ToString("yyyy-MM-dd");
            return strDate;
        }
        //删除过期数据
        private void DeleteOutodDateXML()
        {
            DateTime dt = DateTime.Now;
            XElement xe = XElement.Load(xmlPath);
            IEnumerable<XElement> delete = from del in xe.Elements("Exercise")
                                           where Convert.ToDateTime(del.Element("Date").Value) <= dt.AddDays(-4)
                                           select del;
            if (delete.Count() > 0)
            {
                delete.Remove();
            }
            xe.Save(xmlPath);
        }
    }
}
