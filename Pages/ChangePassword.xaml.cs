﻿using System.Windows;
using System.Windows.Controls;
using System.Data.SqlClient;

namespace HealthManagementCenter.Pages
{
    /// <summary>
    /// ChangePassword.xaml 的交互逻辑
    /// </summary>
    public partial class ChangePassword : Page
    {
        public ChangePassword(string name, MainWindow.UserRole role)
        {
            InitializeComponent();
            if (name == "")
                return;
            _userName = name;
            _role = role;
        }

        //用户名
        private string _userName;
        public string UserName
        {
            get
            {
                return _userName;
            }
        }
        //用户角色
        private MainWindow.UserRole _role;
        public MainWindow.UserRole Role
        {
            get
            {
                return _role;
            }
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (txtNewPw.Password != txtReNewPw.Password)
            {
                MessageBox.Show("两次输入的新密码不一致，请检查后重试！", "错误", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string olePw = txtOldPw.Password;
            string newPw = txtNewPw.Password;
            string md5Password = DataHelper.GetMD5(olePw);
            string md5NewPw=DataHelper.GetMD5(newPw);
            //验证原密码是否正确
            int val = 0,result=0;
            //验证用户有效性
            switch (Role)
            {
                case MainWindow.UserRole.Student:
                    val = (int)SqlHelper.ExecuteScalar("exec LoginValidate @_num,@_pw,@_role", new SqlParameter("@_num", UserName),
                            new SqlParameter("@_pw", md5Password), new SqlParameter("@_role", "student"));
                    break;
                case MainWindow.UserRole.Doctor:
                    val = (int)SqlHelper.ExecuteScalar("exec LoginValidate @_num,@_pw,@_role", new SqlParameter("@_num", UserName),
                            new SqlParameter("@_pw", md5Password), new SqlParameter("@_role", "doctor"));
                    break;
                case MainWindow.UserRole.Admin:
                    val = (int)SqlHelper.ExecuteScalar("exec LoginValidate @_num,@_pw,@_role", new SqlParameter("@_num", UserName),
                            new SqlParameter("@_pw", md5Password), new SqlParameter("@_role", "admin"));
                    break;
                default:
                    break;
            }
            if (val == 1)
            {
                // 验证用户有效性成功后根据不同用户修改密码
                switch (Role)
                {
                    case MainWindow.UserRole.Student:
                        result = SqlHelper.ExecuteNonQuery("update student set password=@pw where snum=@snum", 
                            new SqlParameter("@pw", md5NewPw),new SqlParameter("@snum", UserName));
                        break;
                    case MainWindow.UserRole.Doctor:
                        result = SqlHelper.ExecuteNonQuery("update doctor set password=@pw where docnum=@docnum",
                            new SqlParameter("@pw", md5NewPw), new SqlParameter("@docnum", UserName));
                        break;
                    case MainWindow.UserRole.Admin:
                        val = SqlHelper.ExecuteNonQuery("update [admin] set password=@pw where anum=@anum",
                            new SqlParameter("@pw", md5NewPw), new SqlParameter("@anum", UserName));
                        break;
                    default:
                        break;
                }
                if(val==1)
                {
                    MessageBox.Show("密码修改成功！");
                }
                else
                {
                    MessageBox.Show("密码修改失败！");
                }
            }
            else
            {
                MessageBox.Show("对不起，您输入的原密码不匹配", "错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            txtNewPw.Password = "";
            txtOldPw.Password = "";
            txtReNewPw.Password = "";
        }
    }
}
